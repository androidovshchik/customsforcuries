package ru.newpartner.forcuriers;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.sql.SQLException;

import ru.newpartner.forcuriers.R;

import static ru.newpartner.forcuriers.MainActivity.IntDateToString;

public class Checks_ListActivity extends ActionBarActivity {

    ListView mList;
    ListView mListGoods;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    Cursor userCursorGoods;
    SimpleCursorAdapter userAdapter;
    SimpleCursorAdapter userAdapterGoods;
    int tekdate;
    String tekDoc_Number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checks_list);

        Intent intentShop = getIntent();
        tekdate = intentShop.getIntExtra("tekdate", 0);
        tekDoc_Number = intentShop.getStringExtra("DocNumber");
        Log.i("","Я ЗДЕСЬ в списке документов !!!");
        Log.i("", String.valueOf(tekdate));
        Log.i("",tekDoc_Number);

        TextView mTxtML = (TextView) findViewById(R.id.textViewDocNumber);
        mTxtML.setText("Накладная № "+tekDoc_Number.toString()+" от "+IntDateToString(tekdate, true));

        mList = (ListView) findViewById(R.id.list);
        mListGoods = (ListView) findViewById(R.id.Goodslist);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor tekstr = (Cursor) mList.getItemAtPosition(position);
                String tekNumber = tekstr.getString(tekstr.getColumnIndex("FD"));
//                Intent intent = new Intent(getApplicationContext(), PrintCheckActivity.class);
//                intent.putExtra("CheckNumber", tekNumber);
//                startActivity(intent);

                //выделение строки цветом
                for(int a = 0; a < parent.getChildCount(); a++)
                {
                    LinearLayout TekViewStr = (LinearLayout) parent.getChildAt(a);
                    for (int a1 = 0; a1 < TekViewStr.getChildCount(); a1++) {
                        LinearLayout TekViewStr1 = (LinearLayout) TekViewStr.getChildAt(a1);
                        for (int a2 = 0; a2 < TekViewStr1.getChildCount(); a2++) {
                            if (a == position)
                                TekViewStr1.getChildAt(a2).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_list_edit_style));
                            else
                                TekViewStr1.getChildAt(a2).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_list_style));
                        }
                    }
                }
                //конец выделение строки цветом

                userCursorGoods = sqlHelper.database.rawQuery("select ChecksGoods.* from ChecksGoods" +
                       " where CheckFD = ?", new String[]{tekNumber});

                String[] from = {"GoodsName", "GoodsCount", "GoodsPrice", "GoodsSum"};
                int[] to = {R.id.TVname, R.id.TVcount, R.id.TVprice, R.id.TVSum};
                userAdapterGoods = new Goods_ListMyAdapter(Checks_ListActivity.this, R.layout.layout_goods_list, userCursorGoods, from, to);
                mListGoods.setAdapter(userAdapterGoods);

            }
        });

        Button btnPrint = (Button) findViewById(R.id.buttonPrintCheck);
        btnPrint.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Cursor tekstr = (Cursor) mList.getItemAtPosition(mList.getSelectedItemPosition());
                                            if (tekstr == null)
                                            {tekstr = (Cursor) mList.getItemAtPosition(0);}
                                            String tekNumber = tekstr.getString(tekstr.getColumnIndex("FD"));
                                            Intent intent = new Intent(getApplicationContext(), PrintCheckActivity.class);
                                            intent.putExtra("FD", tekNumber);
                                            startActivity(intent);
                                        }
                                    });


        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

    }


    @Override
    public void onResume() {
        super.onResume();

        try {

            sqlHelper.open();

            userCursor = sqlHelper.database.rawQuery("select Checks._id as _id, Checks.DocNumber as DocNumber, Checks.FD as FD, Checks.CheckSumN+Checks.CheckSumE as Summa, Checks.CheckTime as CheckTime from Checks" +
                    " where DocNumber = ?"+" order by DocNumber", new String[]{tekDoc_Number});

            String[] from = {"FD", "CheckTime", "Summa"};
            int[] to = {R.id.TVnumber, R.id.TVdate, R.id.TVSum};
            userAdapter = new Checks_ListMyAdapter(this, R.layout.layout_checks_list, userCursor, from, to);
            mList.setAdapter(userAdapter);
                    }
        catch (SQLException ex) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
