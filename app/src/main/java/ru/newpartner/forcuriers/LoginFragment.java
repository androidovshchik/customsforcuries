package ru.newpartner.forcuriers;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LoginFragment extends Fragment {
    TextView contentView;
    String contentText = null;
    WebView webView;
    EditText txtLogin;
    EditText txtPassword;
    EditText txtWebServer;
    //Spinner ListWebServer;
    Spinner ListWebServer;
    DatabaseHelper sqlHelper;
    Button btnFetch;
    int resLog;
    static SoapObject TekRes;

    public static final String SOAP_ACTION = "http://delivery-russia.ru#MobileCouriersExchange:GetCouriersKod";
    public static final String METHOD_NAME = "GetCouriersKod";
    public static final String METHOD_NAME_LOAD = "GetCouriersList";
    public static String tekLogin;
    public static String tekPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqlHelper = new DatabaseHelper(this.getActivity());
        // создаем базу данных
        sqlHelper.create_db();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);
        view.findViewById(R.id.layout).requestFocus();
        contentView = (TextView) view.findViewById(R.id.content);
        webView = (WebView) view.findViewById(R.id.webView);
        txtLogin = (EditText) view.findViewById(R.id.editTextLogin);
        txtPassword = (EditText) view.findViewById(R.id.editTextPassword);
        txtWebServer = (EditText) view.findViewById(R.id.editTextIP);
        TekRes = null;

        //заполнение списка веб-серверов
        ListWebServer = (Spinner) view.findViewById(R.id.ListIP);
        // адаптер
        String[] data = {"Москва", "Ярославль"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ListWebServer.setAdapter(adapter);
        ListWebServer.post(new Runnable() {
            @Override
            public void run() {
                ListWebServer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String tekPlace = ListWebServer.getSelectedItem().toString();
                        if (tekPlace.equals("Москва"))
                            txtWebServer.setText("185.46.153.37/sd_msk");
                        else
                            txtWebServer.setText("109.195.118.84/sd_ysd");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        });



//        ListWebServer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                String tekPlace = ListWebServer.getSelectedItem().toString();
//                if (tekPlace.equals("Москва")) {
//                    txtWebServer.setText("95.165.229.218/sd_msk");
//                }
//                else
//                    txtWebServer.setText("109.195.118.84/sd_ysd");
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });

        //восстановление логина и пароля из настроек приложения
        loadText();

        btnFetch = (Button) view.findViewById(R.id.loginBtn);

        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtWebServer.getText().toString().isEmpty())
                {
                    btnFetch.setText("Укажите web-сервер");
                }

                else {
                    contentView.setText("Загрузка данных с веб-сервера...");
                    //сохранение введенного логина и пароля в настройки приложения
                    saveText();
                    resLog = 0;
                    tekLogin = txtLogin.getText().toString();
                    tekPassword = txtPassword.getText().toString();
                    ru.newpartner.forcuriers.MainActivity.URL = "http://" + txtWebServer.getText().toString() + "/ws/MobileCouriersExchange.1cws";

                    //попытка подключиться к веб-серверу с введенным логином и паролоем
                    btnFetch.setText("Выполняется запрос...");
                    btnFetch.setEnabled(false);
                    LoginTask LogTask = new LoginTask();
                    LogTask.execute();
                    //new LoadingTask().execute();
                }

            }
        });

//        Button btnCopy = (Button) view.findViewById(R.id.CopyBtn);
//        btnCopy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onclickCopyDB(view);
//            }
//        });
        return view;
    }

    private class LoginTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content;
            if (isOnline() == true)
            {
                //запрос к веб-сервису для проверки логина и пароля
                content = getContent();
                return content;
            }
            else
            {
                content = "Нет доступа к интернету";
                return content;
            }
        }
        @Override
        protected void onPostExecute(String content) {
            if (getActivity() == null) {
                return;
            }
            contentText=content;
            contentView.setText(content);
            webView.loadData(content, "text/html; charset=utf-8", "utf-8");
            FindCuriers();
            contentView.setText(String.valueOf(MainActivity.TekCurier_id).toString()+";  "+ MainActivity.TekCurier_FIO);
            resLog = 1;
            btnFetch.setText("Маршрутные листы...");
            //загрузка маршрутных листов и наклданых с веб-сервера
            new LoadingTask().execute();
            //btnFetch.setText("Вход");
        }

        private String getContent() {

            String result = "";
            do {
                SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME);
                request.addProperty("Login", txtLogin.getText().toString());
                request.addProperty("Password", txtPassword.getText().toString());

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultLogin = (SoapObject) envelope.getResponse();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultLogin.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultLogin.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName"))
                        {
                            result = resultLogin.getProperty(i).toString();
                            return result;
                        }
                    }
                    result = resultLogin.getProperty("FIO").toString();
                    //загрузка в нашу базу данных курьера, его пароля и ФИО
                    loadCuriers(resultLogin);

                } catch (Exception e) {
                    e.printStackTrace();
                    result = e.toString();
                }
            }
            while (false);
            return result;
        }
    }

    void loadCuriers(SoapObject res) {

        Cursor userCursor;
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        //проверка, имеется ли этот курьер в базе данных уже
        userCursor = db.rawQuery("select _id, CFIO, CPassword from curiers where CLogin " +
            "=?", new String[]{txtLogin.getText().toString()});
        int Kol = userCursor.getCount();
        if (Kol > 1) {   //удаляем из базы все записи по этому логину
            //потом добавим
            db.delete("curiers", "CLogin = ?", new String[]{txtLogin.getText().toString()});
        } else if (Kol == 1) {//замещаем эту запись
            userCursor.moveToFirst();
            ContentValues cv = new ContentValues();
            String TekParametr = res.getProperty("FIO").toString();
            cv.put("CFIO", TekParametr);
            cv.put("CLogin", txtLogin.getText().toString());
            cv.put("CPassword", txtPassword.getText().toString());
            db.update("curiers", cv, "_id" + "=" + String.valueOf(userCursor.getLong(0)), null);
        } else {//добавление нового курьера втаблицу
            ContentValues cv = new ContentValues();
            String TekParametr = res.getProperty("FIO").toString();
            cv.put("CFIO", TekParametr);
            cv.put("CLogin", txtLogin.getText().toString());
            cv.put("CPassword", txtPassword.getText().toString());
            db.insert("curiers", null, cv);
        }
        db.close();
    }

    void FindCuriers() {
        Cursor userCursor;
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        //считываем ид. текущего курьера для его дальнейшего использования
        userCursor = db.rawQuery("select _id, CFIO, CPassword from curiers where CLogin " +
            "=?", new String[]{txtLogin.getText().toString()});
        if (userCursor.getCount()>0) {
            userCursor.moveToFirst();
            MainActivity.TekCurier_id = userCursor.getLong(0);
            MainActivity.TekCurier_FIO = userCursor.getString(1);
            Log.i("", String.valueOf(MainActivity.TekCurier_id));
        }
        else
        {
            MainActivity.TekCurier_id = -1;
            MainActivity.TekCurier_FIO = "Курьер не выбран";
        }
        db.close();
    }

    private class LoadingTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content;
            content = getContent();
            return content;
        }
        @Override
        protected void onPostExecute(String content) {
            if (getActivity() == null) {
                return;
            }
            contentText=content;
            contentView.setText(content);
            webView.loadData(content, "text/html; charset=utf-8", "utf-8");
            btnFetch.setText("ВХОД");
            btnFetch.setEnabled(true);

            //запускаем сервис для опроса web-сервера  для ТЕСТА
            Intent IntetnServise = new Intent(getActivity(), ServiceQueryToServer.class);
            //запускаем аларм менеджер
            PendingIntent pi = PendingIntent.getService(getActivity(),1,IntetnServise,0);
            AlarmManager AlarmM = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            AlarmM.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 300000, pi);

            //  getActivity().startService(IntetnServise);

            Intent intent = new Intent(getActivity(), ML_ListActivity.class);
            startActivity(intent);
        }

        private String getContent()  {

            String result = "";
            do {
                SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME_LOAD);
                request.addProperty("Kod", txtLogin.getText().toString());
                request.addProperty("Days", MainActivity.KOLDAYS);

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultDocs = (SoapObject) envelope.getResponse();
                    result = resultDocs.toString();
                    MainActivity.loadDocs(resultDocs, false, getActivity().getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                    result = e.toString();
                }
            }
            while (false);
            return result;
        }
    }


    void saveText() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        SharedPreferences.Editor ed = preferences.edit();
        ed.putString("Login", txtLogin.getText().toString());
        ed.putString("Password", txtPassword.getText().toString());
        ed.putString("Webserver", txtWebServer.getText().toString());
        ed.putString("Place", ListWebServer.getSelectedItem().toString());
        ed.apply();
    }

    void loadText() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        String wsParam = preferences.getString("Login", "");
        txtLogin.setText(wsParam);
        wsParam = preferences.getString("Password", "");
        txtPassword.setText(wsParam);
        String wsParamWs = preferences.getString("Webserver", "");
        wsParam = preferences.getString("Place", "");
        if (wsParam.equals("Москва") || wsParam.isEmpty() || wsParamWs.isEmpty())
            ListWebServer.setSelection(0);
        else ListWebServer.setSelection(1);
        if (!wsParamWs.isEmpty())
            txtWebServer.setText(wsParamWs);
        else
            txtWebServer.setText("185.46.153.37/sd_msk");
    }

    protected boolean isOnline() {
        String cs = getActivity().CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
            getActivity().getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void  onStop() {
        super.onStop();
        saveText();
    }

    public void onclickCopyDB(View view) {
        //служебная процедура, кнопка не видна в активити
        //копирование из рабочей базы на диск телефона
        InputStream myInput = null;
        OutputStream myOutput = null;
        TextView tvInfoCopy = (TextView) view.findViewById(R.id.CopyBtn);
        tvInfoCopy.setText("Попытка скопировать базу");
        try {

            String DB_PATH = "/data/data/ru.newpartner.forcuriers/databases/";
            String DB_NAME = "DbCuriers";
            File fFrom = new File(DB_PATH + DB_NAME);
            Log.i("", DB_PATH + DB_NAME);
            if (fFrom.exists()) {
                // Путь к новой бд
                String sdState = android.os.Environment.getExternalStorageState(); //Получаем состояние SD карты (подключена она или нет) - возвращается true и false соответственно
                if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) // если true
                {
                    String folder = Environment.getExternalStorageDirectory().toString();
                    Log.i("", folder);
                    //String folder = "c:/AndroidProjects/22";
                    File f1 = new File(folder); //Создаем файловую переменную
                    if (!f1.exists()) { //Если папка не существует
                        f1.mkdirs(); //создаем её
                    }

                    String outFileName = folder +"/" +DB_NAME;
                    myOutput = new FileOutputStream(outFileName);
                    String inFileName = DB_PATH + DB_NAME;
                    myInput = new FileInputStream(inFileName);

                    // побайтово копируем данные
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = myInput.read(buffer)) > 0) {
                        myOutput.write(buffer, 0, length);
                    }

                    myOutput.flush();
                    myOutput.close();
                    myInput.close();
                    tvInfoCopy.setText("База скопирована: "+DB_NAME);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            tvInfoCopy.setText("Копирование не удалось");
        }
    }
}