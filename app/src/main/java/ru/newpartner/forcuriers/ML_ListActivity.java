package ru.newpartner.forcuriers;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.sql.SQLException;
;

public class ML_ListActivity extends ActionBarActivity {

    ListView mList;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    SimpleCursorAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ml_list);

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        if (MainActivity.TekCurier_FIO != null)
            mTxtFIO.setText("Маршрутные листы курьера: " + MainActivity.TekCurier_FIO.toString());
        else
            mTxtFIO.setText("Курьер не выбран");

        mList = (ListView) findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor tekstr = (Cursor) mList.getItemAtPosition(position);
                int tekdatestr = tekstr.getInt(tekstr.getColumnIndex("MLDate"));
                String tekML = tekstr.getString(tekstr.getColumnIndex("MLNumber"));

                Intent intent = new Intent(getApplicationContext(), Docs_ListActivity.class);
                intent.putExtra("tekdate", tekdatestr);
                intent.putExtra("MLNumber", tekML);
                startActivity(intent);
            }
        });


        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            sqlHelper.open();
            userCursor = sqlHelper.database.rawQuery("select max(docs._id) as _id, docs.MLNumber as MLNumber, docs.MLDate as MLDate from Docs where Curier_id = "+ MainActivity.TekCurier_id+" group by docs.MLDate, docs.MLNumber", null);

            String[] from = {"MLNumber", "MLDate"};
            int[] to = {R.id.TVdate, R.id.TVnumber};
            userAdapter = new ML_ListMyAdapter(this, R.layout.layout_ml_list, userCursor, from, to);
            mList.setAdapter(userAdapter);
                    }
        catch (SQLException ex) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_show_messages:
                Intent MessagesIntent = new Intent(this, Messages_ListActivity.class);
                MessagesIntent.putExtra("IsNew",0);
                startActivity(MessagesIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

}
