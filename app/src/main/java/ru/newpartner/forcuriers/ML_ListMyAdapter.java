package ru.newpartner.forcuriers;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import ru.newpartner.forcuriers.R;

public class ML_ListMyAdapter extends SimpleCursorAdapter {

    private int layout;

    public ML_ListMyAdapter(Context _context, int _layout, Cursor _cursor, String[] _from, int[] _to) {
        super(_context, _layout, _cursor, _from, _to);
        layout = _layout;
    }


    //связывает данные с view на которые указывает курсор
    @Override
    public void bindView(View view, Context _context, Cursor _cursor) {
        int ColumnDate = _cursor.getInt(_cursor.getColumnIndex("MLDate"));
        String ColumnNumber = _cursor.getString(_cursor.getColumnIndex("MLNumber"));
        TextView dateTV = (TextView) view.findViewById(R.id.TVdate);
        TextView totalsumTV = (TextView) view.findViewById(R.id.TVnumber);

        //преобразование даты из целого в строку
       // int tekdate = Integer.valueOf(ColumnDate);
        String tekdateStr = MainActivity.IntDateToString(ColumnDate, true);
        dateTV.setText(tekdateStr);

        totalsumTV.setText(ColumnNumber);
    }

    //сoздаёт нвоую view для хранения данных на которую указывает курсор
    @Override
    public View newView(Context _context, Cursor _cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout, parent, false);
        return view;
    }

}
