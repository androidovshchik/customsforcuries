package ru.newpartner.forcuriers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class Docs_ListMyAdapter extends SimpleCursorAdapter {

    private final int layout;

    private final int defaultFontSize;

    private final SharedPreferences preferences;

    public Docs_ListMyAdapter(Context _context, int _layout, Cursor _cursor, String[] _from, int[] _to) {
        super(_context, _layout, _cursor, _from, _to);
        layout = _layout;
        Resources resources = _context.getResources();
        preferences = PreferenceManager.getDefaultSharedPreferences(_context);
        defaultFontSize = Math.round(resources.getDimensionPixelSize(R.dimen.textSize)
            / resources.getDisplayMetrics().density);
    }

    //связывает данные с view на которые указывает курсор
    @Override
    public void bindView(View view, Context _context, Cursor _cursor) {
        String ColumnAdr = _cursor.getString(_cursor.getColumnIndex("Address"));
        String ColumnNumber = _cursor.getString(_cursor.getColumnIndex("DocNumber"));
        String ColumnZNumber = _cursor.getString(_cursor.getColumnIndex("DocZNumber"));
        int ColumnZabor = _cursor.getInt(_cursor.getColumnIndex("DocIsDelivery"));
        int ColumnPOD = _cursor.getInt(_cursor.getColumnIndex("POD"));
        String ColumnSum = _cursor.getString(_cursor.getColumnIndex("GoodsSum"));
        String ColumnFIO = _cursor.getString(_cursor.getColumnIndex("FIO"));
        String ColumnPhone = _cursor.getString(_cursor.getColumnIndex("Phone"));
        String ColumnCompany = _cursor.getString(_cursor.getColumnIndex("Company"));
        LinearLayout dragHandle = (LinearLayout) view.findViewById(R.id.drag_handle);
        TextView dateTV = (TextView) view.findViewById(R.id.TVdate);
        TextView sumTV = (TextView) view.findViewById(R.id.TVSum);
        TextView totalsumTV = (TextView) view.findViewById(R.id.TVnumber);
        TextView ZNumber = (TextView) view.findViewById(R.id.TVZnumber);
        TextView Zabor = (TextView) view.findViewById(R.id.TVZabor);
        ImageView ImTV2 = (ImageView) view.findViewById(R.id.imageView2);
        ImageView ImTV3 = (ImageView) view.findViewById(R.id.imageView3);
        TextView fioTV = (TextView) view.findViewById(R.id.TVFIO);
        TextView phoneTV = (TextView) view.findViewById(R.id.TVPhone);
        TextView companyTV = (TextView) view.findViewById(R.id.TVCompany);

        dateTV.setText(ColumnAdr);
        sumTV.setText(ColumnSum);
        fioTV.setText(ColumnFIO);
        phoneTV.setText(ColumnPhone);
        ZNumber.setText(ColumnZNumber);
        companyTV.setText(ColumnCompany);
        companyTV.setVisibility(ColumnCompany != null && !ColumnCompany.trim().isEmpty() ?
            View.VISIBLE : View.GONE);
        totalsumTV.setText(ColumnNumber);
        totalsumTV.setVisibility(ColumnNumber != null && !ColumnNumber.trim().isEmpty() ?
            View.VISIBLE : View.GONE);
        boolean needChangeBackground = true;
        if (ColumnPOD == 1) {
            dragHandle.setBackgroundColor(_context.getResources().getColor(R.color.colorDone));
            needChangeBackground = false;
        }
        if (ColumnZabor == 1) {
            ImTV3.setImageResource(R.drawable.ic_arrow_upward_24dp);
            Zabor.setText("Дост.");
            if (needChangeBackground) {
                dragHandle.setBackgroundColor(_context.getResources().getColor(R.color.colorDostavka));
            }
        } else if (ColumnZabor == 2) {
            ImTV3.setImageResource(R.drawable.ic_person_24dp);
            Zabor.setText("Вызов");
            if (needChangeBackground) {
                dragHandle.setBackgroundColor(_context.getResources().getColor(R.color.colorVusov));
            }
        } else {
            ImTV3.setImageResource(R.drawable.ic_arrow_downward_24dp);
            Zabor.setText("Забор");
            if (needChangeBackground) {
                dragHandle.setBackgroundColor(_context.getResources().getColor(R.color.colorZabor));
            }
        }
        if (ColumnPOD == 1) {
            ImTV2.setImageResource(R.drawable.ic_done_24dp);
        } else if (ColumnPOD == 2) {
            ImTV2.setImageResource(R.drawable.ic_add_circle_24dp);
        } else {
            ImTV2.setImageResource(R.drawable.ic_directions_car_24dp);
        }

        loopViews((ViewGroup) view, preferences.getInt("fontSize", defaultFontSize));
    }

    private void loopViews(ViewGroup view, int fontSize) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTextSize(fontSize);
            } else if (v instanceof ViewGroup) {
                this.loopViews((ViewGroup) v, fontSize);
            }
        }
    }

    //сoздаёт нвоую view для хранения данных на которую указывает курсор
    @Override
    @SuppressWarnings("ConstantConditions")
    public View newView(Context _context, Cursor _cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
            _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, parent, false);
    }
}
