package ru.newpartner.forcuriers;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;

import static ru.newpartner.forcuriers.MainActivity.TekCurier_FIO;

public class Docs_New_ListActivity extends ActionBarActivity {

    ListView mList;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    SimpleCursorAdapter userAdapter;
    int tekdate;
    String tekML_Number;
    TextView KolDocs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs_new_list);

        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

        Intent intentShop = getIntent();
        tekdate = intentShop.getIntExtra("tekdate", 0);
        tekML_Number = intentShop.getStringExtra("MLNumber");

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        if (MainActivity.TekCurier_FIO == null)
            mTxtFIO.setText("Необходимо войти в приложение, указав логин и пароль !");
        else
            mTxtFIO.setText(TekCurier_FIO.toString());

        TextView mTxtML = (TextView) findViewById(R.id.textViewML);
//        mTxtML.setText("Маршр.лист "+tekML_Number.toString()+" от "+IntDateToString(tekdate, true));

        KolDocs = (TextView) findViewById(R.id.textViewKolDocs);

        mList = (ListView) findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                showPopupMenu(view, position);

                }

//                int tekdate = Integer.valueOf(tekdatestr);
//
//                Intent intent = new Intent(getApplicationContext(), DayShopsActivity.class);
//                intent.putExtra("tekdate", tekdate);
//                startActivity(intent);

        });

    }

    private void showPopupMenu(View v, final int tek_position) {

        final int tek_pos = tek_position;
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.menu_accept_doc);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Toast.makeText(PopupMenuDemoActivity.this,
                // item.toString(), Toast.LENGTH_LONG).show();
                // return true;
                switch (item.getItemId()) {

                    case R.id.menu_accept_one:
                        AcceptDocs(1, tek_pos);
                        Toast.makeText(getApplicationContext(),
                                "Принята одна заявка",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.menu_accept_all:
                        AcceptDocs(0, 0);
                        Toast.makeText(getApplicationContext(),
                                "Приняты все заявки",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private void AcceptDocs(int TekAction, int tek_position) {

        if (TekAction == 1) {
            Cursor tekstr = (Cursor) mList.getItemAtPosition(tek_position);
            Long tek_id = tekstr.getLong(tekstr.getColumnIndex("_id"));

            Cursor userCursor1 = sqlHelper.database.rawQuery("select * from Docs where _id = " + tek_id, null);
            if (userCursor1.getCount() > 0) {
                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                String sql = "update Docs set isNew = 0 where _id =" + tek_id + ";";
                db.execSQL(sql);
                db.close();
            }
        }
        else {
        //ставим признак принято у всех новых заявок
                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                String sql = "update Docs set isNew = 0 where isNew = 1;";
                db.execSQL(sql);
                db.close();
        }
    }


        @Override
    public void onResume() {
        super.onResume();

        try {
            sqlHelper.open();
            userCursor = sqlHelper.database.rawQuery("select docs._id as _id, docs.DocNumber as DocNumber, docs.DocZNumber as DocZNumber, docs.Address as Address, " +
                    "docs.DocIsDelivery as DocIsDelivery, docs.isGoods as isGoods, docs.EventDateTime as EventDateTime, docs.Instruction as Instruction from Docs" +
                    " where isNew = 1 order by TripNumber", null);
            String[] from = {"EventDateTime", "DocNumber", "DocZNumber", "Address", "DocIsDelivery", "Instruction"};
            int[] to = {R.id.TVdatetime, R.id.TVdate, R.id.TVnumber, R.id.TVZnumber, R.id.TVZabor, R.id.TVinstruction};
            userAdapter = new Docs_New_ListMyAdapter(this, R.layout.layout_docs_new_list, userCursor, from, to);
            mList.setAdapter(userAdapter);

            //выводим кол-во накладных
            KolDocs.setText("Кол-во новых заданий: "+String.valueOf(userCursor.getCount()));

                    }
        catch (SQLException ex) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
