package ru.newpartner.forcuriers;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import ru.newpartner.forcuriers.R;

public class Messages_ListMyAdapter extends SimpleCursorAdapter {

    private int layout;

    public Messages_ListMyAdapter(Context _context, int _layout, Cursor _cursor, String[] _from, int[] _to) {
        super(_context, _layout, _cursor, _from, _to);
        layout = _layout;
    }


    //связывает данные с view на которые указывает курсор
    @Override
    public void bindView(View view, Context _context, Cursor _cursor) {
        String ColumnDateTime = _cursor.getString(_cursor.getColumnIndex("DateTimeStr"));
        String ColumnSender = _cursor.getString(_cursor.getColumnIndex("Sender"));
        String ColumnText = _cursor.getString(_cursor.getColumnIndex("MessageText"));
        int ColumnIsNew = _cursor.getInt(_cursor.getColumnIndex("IsNew"));
        TextView dateTV = (TextView) view.findViewById(R.id.TVdate);
        TextView senderTV = (TextView) view.findViewById(R.id.TVsender);
        TextView textTV = (TextView) view.findViewById(R.id.TVText);
        ImageView ImTV = (ImageView) view.findViewById(R.id.imageView2);

        dateTV.setText(ColumnDateTime);
        senderTV.setText("от: "+ColumnSender);
        textTV.setText(ColumnText);
        if (ColumnIsNew == 1)
            ImTV.setImageResource(R.drawable.ic_mail_24dp);
        else  ImTV.setImageResource(R.drawable.ic_drafts_24dp);

    }

    //сoздаёт новую view для хранения данных на которую указывает курсор
    @Override
    public View newView(Context _context, Cursor _cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout, parent, false);
        return view;
    }

}
