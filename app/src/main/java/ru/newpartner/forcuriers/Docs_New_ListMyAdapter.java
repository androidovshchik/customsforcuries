package ru.newpartner.forcuriers;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import ru.newpartner.forcuriers.R;

public class Docs_New_ListMyAdapter extends SimpleCursorAdapter {

    private int layout;

    public Docs_New_ListMyAdapter(Context _context, int _layout, Cursor _cursor, String[] _from, int[] _to) {
        super(_context, _layout, _cursor, _from, _to);
        layout = _layout;
    }


    //связывает данные с view на которые указывает курсор
    @Override
    public void bindView(View view, Context _context, Cursor _cursor) {
        Long ColumnDateTime = _cursor.getLong(_cursor.getColumnIndex("EventDateTime"));
        String ColumnAdr = _cursor.getString(_cursor.getColumnIndex("Address"));
        String ColumnInstr = _cursor.getString(_cursor.getColumnIndex("Instruction"));
        String ColumnNumber = _cursor.getString(_cursor.getColumnIndex("DocNumber"));
        String ColumnZNumber = _cursor.getString(_cursor.getColumnIndex("DocZNumber"));
        int ColumnZabor = _cursor.getInt(_cursor.getColumnIndex("DocIsDelivery"));
        TextView datetimeTV = (TextView) view.findViewById(R.id.TVdatetime);
        TextView dateTV = (TextView) view.findViewById(R.id.TVdate);
        TextView totalsumTV = (TextView) view.findViewById(R.id.TVnumber);
        TextView ZNumber = (TextView) view.findViewById(R.id.TVZnumber);
        TextView Zabor = (TextView) view.findViewById(R.id.TVZabor);
        TextView Instr = (TextView) view.findViewById(R.id.TVinstruction);

        datetimeTV.setText(MainActivity.IntDateTimeToString(ColumnDateTime, true));
        dateTV.setText(ColumnAdr);
        Instr.setText(ColumnInstr);
        totalsumTV.setText(ColumnNumber);
        ZNumber.setText(ColumnZNumber);
        if (ColumnZabor == 1) Zabor.setText("Д");
        else if (ColumnZabor == 2) Zabor.setText("В");
        else Zabor.setText("З");
    }

    //сoздаёт нвоую view для хранения данных на которую указывает курсор
    @Override
    public View newView(Context _context, Cursor _cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout, parent, false);
        return view;
    }

}
