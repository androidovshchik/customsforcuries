package ru.newpartner.forcuriers;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    public static final String NAMESPACE = "http://delivery-russia.ru";
    //это для Ярославля
    public static final int KOLDAYS = 7;
    //public static final String URL = "http://109.195.118.84/sd_ysd/ws/MobileCouriersExchange.1cws";

    //это для Москвы
    //public static final int KOLDAYS = 7;
    // public static final String URL = "http://95.165.229.218/sd_msk/ws/MobileCouriersExchange.1cws";
    public static String URL;
    public static final String user = "MobiUser";
    public static final String pass = "1597534682";

    public static long TekCurier_id;
    public static String TekCurier_FIO;
    public static Boolean isSortDocs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TekCurier_id = -1;
        TekCurier_FIO = "Курьер не выбран";
        String vname = BuildConfig.VERSION_NAME;
        setTitle("Сервис для курьеров  v"+vname);
        isSortDocs = false;
        //  Log.d("", "ЛУНЕВА onCreate");
    }

    public static String IntDateToString(int tekDate, boolean kratko) {

        int myDay = tekDate % 100;
        int tekdate1 = tekDate / 100;
        int myMonth = tekdate1 % 100;
        int myYear = tekdate1 / 100;
        GregorianCalendar cal = new GregorianCalendar(myYear, myMonth - 1, myDay);
        SimpleDateFormat df;
        if (kratko == true) {
            df = new SimpleDateFormat("dd.MM.yyyy");
        } else {
            df = new SimpleDateFormat("dd MMMM yyyy");
        }
        String dt = df.format(cal.getTime());
        return (dt.toString());

    }

    public static String IntDateTimeToString(Long tekDateTime, boolean kratko) {

        int myMinute = (int) ((tekDateTime % 10000) / 100);
        int myHour = (int) ((tekDateTime % 1000000) / 10000);
        int myDay = (int) ((tekDateTime % 100000000) / 1000000);
        int tekdate1 = (int) (tekDateTime / 100000000);
        int myMonth = tekdate1 % 100;
        int myYear = tekdate1 / 100;
        GregorianCalendar cal = new GregorianCalendar(myYear, myMonth - 1, myDay, myHour, myMinute);
        SimpleDateFormat df;
        if (kratko == true) {
            df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        } else {
            df = new SimpleDateFormat("dd MMMM yyyy");
        }
        String dt = df.format(cal.getTime());
        return (dt.toString());

    }

    public static String loadCheck(SoapObject res, String tekDoc_Number, Context _context) {

        DatabaseHelper sqlHelper = new DatabaseHelper(_context);
        // создаем базу данных
        sqlHelper.create_db();

        Log.i("", "Добавляем записи в чек");
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        String CheckFD = res.getProperty("FD").toString();
        db.delete("Checks", "DocNumber = ?", new String[]{tekDoc_Number});
        ContentValues cv = new ContentValues();
        cv.put("DocNumber", res.getProperty("DocNumber").toString());
        cv.put("CheckNumber", res.getProperty("CheckNumber").toString());
        cv.put("Cashier", res.getProperty("Cashier").toString());
        Log.i("", "Добавляем записи в чек 1");
        cv.put("INN", res.getProperty("INN").toString());
        cv.put("KKtNumber", res.getProperty("KKtNumber").toString());
        cv.put("SmenaNumber", Integer.parseInt(res.getProperty("SmenaNumber").toString()));
        Log.i("", "Добавляем записи в чек 2");
        cv.put("CheckAddress", res.getProperty("CheckAddress").toString());
        //cv.put("CheckAddress","Адрес пока не заполнен");
        cv.put("CheckTime", res.getProperty("CheckTime").toString());
        cv.put("CheckTimeStr", res.getProperty("CheckTimeStr").toString());
        Log.i("", "Добавляем записи в чек 3");
        cv.put("KktRN", res.getProperty("KktRN").toString());
        Log.i("", "Добавляем записи в чек 31");
        cv.put("FN", res.getProperty("FN").toString());
        Log.i("", "Добавляем записи в чек 32");
        cv.put("FD", res.getProperty("FD").toString());
        Log.i("", "Добавляем записи в чек 33");
        cv.put("FPD", res.getProperty("FPD").toString());
        Log.i("", "Добавляем записи в чек 4");
        cv.put("Organization", res.getProperty("Organization").toString());
        cv.put("CheckSumN", Double.parseDouble(res.getProperty("CheckSumN").toString()));
        cv.put("CheckSumE", Double.parseDouble(res.getProperty("CheckSumE").toString()));
        cv.put("CheckType", Integer.parseInt(res.getProperty("CheckType").toString()));
        db.insert("Checks", null, cv);

        //добавление списка товаров в чек
        db.delete("ChecksGoods", "CheckFD = ?", new String[]{CheckFD});
        //тест
        int count = res.getPropertyCount();
        for (int i = 0; i < count; i++) {
            PropertyInfo pi = new PropertyInfo();
            res.getPropertyInfo(i, pi);
            if (pi.getName().equals("Goods")) {
                SoapObject TekGood = (SoapObject) res.getProperty(i);
                ContentValues cvGood = new ContentValues();
                cvGood.put("CheckFD", CheckFD);
                cvGood.put("GoodsName", TekGood.getProperty("GoodsName").toString());
                cvGood.put("GoodsCount", Double.parseDouble(TekGood.getProperty("GoodsCount").toString()));
                cvGood.put("GoodsPrice", Double.parseDouble(TekGood.getProperty("GoodsPrice").toString()));
                cvGood.put("GoodsSum", Double.parseDouble(TekGood.getProperty("GoodsSum").toString()));
                cvGood.put("GoodsNDS", Integer.parseInt(TekGood.getProperty("GoodsNDS").toString()));
                cvGood.put("GoodsSumNDS", Double.parseDouble(TekGood.getProperty("GoodsSumNDS").toString()));
                cvGood.put("SectionNumber", TekGood.getProperty("SectionNumber").toString());
                db.insert("ChecksGoods", null, cvGood);
            }
        }
        db.close();
        //в 1с есть пробитый чек по накладной, загрузили чек из 1с, возвращаем -1
        return CheckFD;
    }


    public static void loadDocs(SoapObject res, boolean IsNewDocs, Context _context) {

        DatabaseHelper sqlHelper = new DatabaseHelper(_context);
        // создаем базу данных
        sqlHelper.create_db();

        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        long MaxDate = 0;
        //очистка базы данных
        //в дальнейшем сюда добавить условие на ид. курьера. Только его записи удалять
        //String SqlText = "date = ? and shop_id = ? and checktime = ?";
        //db.delete("Docs", SqlText, new String[]{String.valueOf(tekdate), String.valueOf(shop_id), checktime});
        if (!IsNewDocs) {
            //это первичная загрузка всех маршрутных листов, поэтому заново переписываем базу данных
            db.delete("Docs", "", null);
            db.delete("Goods", "", null);

            //удаляем все старые сообщения, которые больше 7 дней
            Calendar cal = (Calendar) Calendar.getInstance();
            // cal.add(Calendar.DAY_OF_MONTH, -7); //пока будем удалять сообщения все кроме сегодняшних, поэтому закомментарила эту строку
            int mYear = cal.get(Calendar.YEAR);
            int mMonth = cal.get(Calendar.MONTH)+1;
            int mDay = cal.get(Calendar.DAY_OF_MONTH);
            int MinDateInt = mYear*10000+mMonth*100+mDay;
            db.delete("Messages", "DateTime/1000000 < "+MinDateInt+" and IsAnswered = 1", null);
        }

        int count1 = res.getPropertyCount();
        if (count1 > 0)
        {
            String resStr = res.getProperty(0).toString();

            try {
                JSONObject resJson = new JSONObject(resStr);
                int countML = resJson.length();
                for (int iml = 1; iml <= countML; iml++) {
                    //JSONObject TekML = resJson.getJSONObject("DocsML_"+String.valueOf(iml));
                    // JSONObject TekML = resJson.getJSONObject("DocsML");
                    // String TekNumberML = TekML.getString("MLNumber");
                    // String TekDateML = TekML.getString("MLDateStr");
                    // JSONObject TekDocs = TekML.getJSONObject("DocsNakl");
                    JSONObject TekDocs = resJson.getJSONObject("DocsML");
                    int countNakl = TekDocs.length();
                    for (int inakl = 1; inakl <= countNakl; inakl++) {
                        ContentValues cv = new ContentValues();
                        JSONObject TeNakl = TekDocs.getJSONObject("Doc_" + String.valueOf(inakl));
                        String TecMLNumber = "00";
                        String TekParametr = TeNakl.getString("MLNumber");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("MLNumber", TekParametr);
                            TecMLNumber = TekParametr;
                        }
                        // cv.put("MLNumber", TekNumberML);
                        TekParametr = TeNakl.getString("DocNumber");
                        String TecDocNumber = "00";
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("DocNumber", TekParametr);
                            TecDocNumber = TekParametr;
                        }
                        String TecDocZNumber = "";
                        TekParametr = TeNakl.getString("ZayavkaNumber");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("DocZNumber", TekParametr);
                            TecDocZNumber = TekParametr;
                        }
                        TekParametr = TeNakl.getString("Address");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("Address", TekParametr);
                        }
                        TekParametr = TeNakl.getString("FIO");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("FIO", TekParametr);
                        }
                        TekParametr = TeNakl.getString("Phone");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("Phone", TekParametr);
                        }
                        TekParametr = TeNakl.getString("Company");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("Company", TekParametr);
                        }
                        TekParametr = TeNakl.getString("Instruction");
                        if (!TekParametr.equals("anyType{}")) {
                            cv.put("Instruction", TekParametr);
                        }
                        TekParametr = TeNakl.getString("MLDateStr");
                        int MLDate = Integer.parseInt(TekParametr);
                        //int MLDate = Integer.parseInt(TekDateML);
                        cv.put("MLDate", MLDate);
                        TekParametr = TeNakl.getString("MLClose");
                        if (!TekParametr.isEmpty()) {
                            int MLClose = Integer.parseInt(TekParametr);
                            cv.put("MLClose", MLClose);
                        } else cv.put("MLClose", 0);
                        TekParametr = TeNakl.getString("DocType");
                        int DocType = Integer.parseInt(TekParametr);
                        cv.put("DocIsDelivery", DocType);
                        TekParametr = TeNakl.getString("TripNumber");
                        if (!TekParametr.isEmpty()) {
                            int TripNumber = Integer.parseInt(TekParametr);
                            cv.put("TripNumber", TripNumber);
                        } else cv.put("TripNumber", 0);
                        TekParametr = TeNakl.getString("IsGoods");
                        if (!TekParametr.isEmpty()) {
                            int IsGoods = Integer.parseInt(TekParametr);
                            cv.put("IsGoods", IsGoods);
                        } else cv.put("IsGoods", 0);
                        TekParametr = TeNakl.getString("POD");
                        if (!TekParametr.isEmpty()) {
                            int POD = Integer.parseInt(TekParametr);
                            cv.put("POD", POD);
                        } else cv.put("POD", 0);
                        if (IsNewDocs) cv.put("IsNew", 1);
                        else cv.put("IsNew", 0);
                        cv.put("Curier_id", TekCurier_id);

                        //запоминаем максимально загруженный период по событиям накладной
                        TekParametr = TeNakl.getString("PeriodStr");
                        long MaxDateNew = Long.parseLong(TekParametr);
                        cv.put("EventDateTime", MaxDateNew);
                        if (MaxDateNew > MaxDate) {
                            MaxDate = MaxDateNew;
                        }

                        if (IsNewDocs) {
                            Cursor userCursorDoc = db.rawQuery("select _id from Docs where MLNumber = ? and DocNumber = ? and DocZNumber = ?", new String[]{TecMLNumber, TecDocNumber, TecDocZNumber});
                            if (userCursorDoc.getCount() > 0)
                                db.update("Docs", cv, "MLNumber = ? and DocNumber = ? and DocZNumber = ?", new String[]{TecMLNumber, TecDocNumber, TecDocZNumber});
                            else db.insert("Docs", null, cv);
                        } else db.insert("Docs", null, cv);

                        //добавление товаров из накладной
                        db.delete("Goods", "DocNumber=?", new String[]{TecDocNumber});
                        JSONObject TekGoods = TeNakl.getJSONObject("Goods");
                        int countGoods = TekGoods.length();
                        for (int igoods = 1; igoods <= countGoods; igoods++) {
                            ContentValues cvGoods = new ContentValues();
                            JSONObject Good = TekGoods.getJSONObject("Goods_" + String.valueOf(igoods));
                            cvGoods.put("DocNumber", TecDocNumber);
                            TekParametr = Good.getString("GoodsName");
                            cvGoods.put("GoodsName", TekParametr);
                            TekParametr = Good.getString("GoodsCount");
                            if (!TekParametr.isEmpty())
                                cvGoods.put("GoodsCount", Double.parseDouble(TekParametr));
                            else cvGoods.put("GoodsCount", 0);
                            TekParametr = Good.getString("GoodsPrice");
                            if (!TekParametr.isEmpty())
                                cvGoods.put("GoodsPrice", Double.parseDouble(TekParametr));
                            else cvGoods.put("GoodsPrice", 0);
                            TekParametr = Good.getString("GoodsSum");
                            if (!TekParametr.isEmpty())
                                cvGoods.put("GoodsSum", Double.parseDouble(TekParametr));
                            else cvGoods.put("GoodsSum", 0);
                            TekParametr = Good.getString("GoodsNDS");
                            if (!TekParametr.isEmpty())
                                cvGoods.put("GoodsNDS", Double.parseDouble(TekParametr));
                            else cvGoods.put("GoodsNDS", 0);
                            TekParametr = Good.getString("GoodsSumNDS");
                            if (!TekParametr.isEmpty())
                                cvGoods.put("GoodsSumNDS", Double.parseDouble(TekParametr));
                            else cvGoods.put("GoodsSumNDS", 0);
                            db.insert("Goods", null, cvGoods);
                        }
                    }
                }

            } catch (JSONException e) {
            }

        }

        //записываем курьеру дату последнего события в МЛ
        ContentValues cvCur = new ContentValues();
        Cursor userCursorCur = db.rawQuery("select _id, CLogin, CFIO, CPassword, LastDocTime  from curiers where _id = " + TekCurier_id, null);
        if (userCursorCur.getCount() > 0) {
            userCursorCur.moveToFirst();
            if (userCursorCur.getLong(4) < MaxDate) {
                cvCur.put("CLogin", userCursorCur.getString(1));
                cvCur.put("CFIO", userCursorCur.getString(2));
                cvCur.put("CPassword", userCursorCur.getString(3));
                cvCur.put("LastDocTime", MaxDate);
                db.update("curiers", cvCur, "_id = " + TekCurier_id, null);
            }
        }
        //очистка чеков и товаров по чекам
        db.delete("Checks", "", null);
        db.delete("ChecksGoods", "", null);
        db.close();

    }

    public static void loadMessages(SoapObject res, Context _context) {

        DatabaseHelper sqlHelper = new DatabaseHelper(_context);
        // создаем базу данных
        sqlHelper.create_db();
        Log.i("", "Добавляем записи в сообщения");
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        int count1 = res.getPropertyCount();
        if (count1 > 0)
        {
            String resStr = res.getProperty(0).toString();
            try {
                JSONObject resJson = new JSONObject(resStr);
                JSONObject Messages = resJson.getJSONObject("SMS");
                int countMes = Messages.length();
                for (int imes = 1; imes <= countMes; imes++) {
                    JSONObject TekMessage = Messages.getJSONObject("SMS_" + String.valueOf(imes));
                    String TekParametr = TekMessage.getString("ID");
                    int TekMessageID = Integer.parseInt(TekParametr);
                    Cursor userCursorMes = db.rawQuery("select _id from Messages where MessageID = " + TekMessageID, null);
                    if (userCursorMes.getCount() == 0) {
                        ContentValues cv = new ContentValues();
                        TekParametr = TekMessage.getString("Text");
                        cv.put("MessageText", TekParametr);
                        TekParametr = TekMessage.getString("DateStr");
                        cv.put("DateTimeStr", TekParametr);
                        TekParametr = TekMessage.getString("Sender");
                        cv.put("Sender", TekParametr);
                        TekParametr = TekMessage.getString("Date");
                        cv.put("DateTime", Long.parseLong(TekParametr));
                        cv.put("MessageID", TekMessageID);
                        cv.put("IsNew", 1);
                        cv.put("Curier_id", TekCurier_id);
                        db.insert("Messages", null, cv);
                    }
                }
            } catch (JSONException e) {
            }
            db.close();

        }
    }

    public static void loadAnsweredMessages(SoapObject res, Context _context) {

        int count1 = res.getPropertyCount();
        if (count1 > 0) {
            String resStr = res.getProperty(0).toString();
            try {
                JSONObject resJson = new JSONObject(resStr);
                JSONObject Messages = resJson.getJSONObject("SMS");
                int countMes = Messages.length();
                if (countMes > 0) {
                    DatabaseHelper sqlHelper = new DatabaseHelper(_context);
                    // создаем базу данных
                    sqlHelper.create_db();
                    SQLiteDatabase db = sqlHelper.getWritableDatabase();
                    for (int imes = 1; imes <= countMes; imes++) {
                        JSONObject TekMessage = Messages.getJSONObject("SMS_" + String.valueOf(imes));
                        String TekParametr = TekMessage.getString("ID");
                        int TekMessageID = Integer.parseInt(TekParametr);
                        String sql = "update Messages set isAnswered = 1 where MessageID = " + TekMessageID + " and Curier_id = " + TekCurier_id + ";";
                        db.execSQL(sql);
                    }
                    db.close();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void loadAnsweredEvents(SoapObject res, Context _context) {
        int count1 = res.getPropertyCount();
        if (count1 > 0) {
            String resStr = res.getProperty(0).toString();
            try {
                JSONObject resJson = new JSONObject(resStr);
                JSONObject Docs = resJson.getJSONObject("Docs");
                DatabaseHelper sqlHelper = new DatabaseHelper(_context);
                // создаем базу данных
                sqlHelper.create_db();
                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                String TekNumber = Docs.getString("Number");
                String TekZNumber = Docs.getString("ZNumber");
                Boolean TekStatus = Docs.getBoolean("Status");
                String TekEvent = Docs.getString("EVENT");
                if (TekStatus == true) {
                    String sql = "";
                    if (TekEvent.contains("Доставлено")) {
                        sql = "update Docs set POD = 1 where DocNumber = ?;";
                        db.execSQL(sql, new String[]{TekNumber});
                    }
                    else if (!TekNumber.isEmpty()) {
                        sql = "update Docs set POD = 2 where DocNumber = ?;";
                        db.execSQL(sql, new String[]{TekNumber});
                    }
                    else if (!TekZNumber.isEmpty()) {
                        sql = "update Docs set POD = 2 where DocZNumber = ?;";
                        db.execSQL(sql, new String[]{TekZNumber});
                    }
                }
                db.close();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Log.d("", "ЛУНЕВА onSaveInstanceState");
        outState.putLong("TekCurier_id", TekCurier_id);
        outState.putString("TekCurier_FIO", TekCurier_FIO);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Log.d("", "ЛУНЕВА onRestoreInstanceState");
        TekCurier_id = savedInstanceState.getLong("TekCurier_id");
        TekCurier_FIO = savedInstanceState.getString("TekCurier_FIO");
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        stopService(new Intent(MainActivity.this, ServiceQueryToServer.class));

    }
}