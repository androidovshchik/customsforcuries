package ru.newpartner.forcuriers;

    import android.app.Notification;
    import android.app.NotificationManager;
    import android.app.PendingIntent;
    import android.app.Service;
    import android.content.Intent;
    import android.content.res.Resources;
    import android.database.Cursor;
    import android.database.sqlite.SQLiteDatabase;
    import android.graphics.BitmapFactory;
    import android.graphics.Color;
    import android.net.ConnectivityManager;
    import android.os.IBinder;
    import android.util.Log;

    import org.json.JSONException;
    import org.json.JSONObject;
    import org.ksoap2.SoapEnvelope;
    import org.ksoap2.serialization.MarshalBase64;
    import org.ksoap2.serialization.PropertyInfo;
    import org.ksoap2.serialization.SoapObject;
    import org.ksoap2.serialization.SoapSerializationEnvelope;
    import org.ksoap2.transport.HttpTransportSE;


public class ServiceQueryToServer extends Service {

    final String LOG_TAG = "myLogs";
    public static final String SOAP_ACTION = "http://delivery-russia.ru#MobileCouriersExchange:GetNewDocs";
    public static final String METHOD_NAME_NEWDOCS = "GetNewDocs";
    public static final String METHOD_NAME_NEWMESSAGES = "GetNewSMS";
    public static final String METHOD_NAME_ANSWEREDMESSAGES = "SetSMSRead";
    public static final String METHOD_NAME_SETTRIPNUMBER = "SetDocTripNumber";

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        someTask();
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    void someTask() {

        new Thread(new Runnable() {
            public void run() {
                if (MainActivity.TekCurier_id != 0L) {
                    if (isOnline() == true) {
                        String answer1C = GetNewDocsFromServer();
                    }
                    DatabaseHelper sqlHelper1 = new DatabaseHelper(ServiceQueryToServer.this);
                    SQLiteDatabase db1 = sqlHelper1.getWritableDatabase();
                    Cursor userCursorDocs = db1.rawQuery("select _id, DocNumber from Docs where IsNew = 1 and Curier_id = " + MainActivity.TekCurier_id, null);
                    if (userCursorDocs.getCount() > 0) {
//                        //оповещаем о приходе новых документов
                        Intent notificationIntent = new Intent(ServiceQueryToServer.this, Docs_New_ListActivity.class);
                        PendingIntent contentIntent = PendingIntent.getActivity(ServiceQueryToServer.this,
                                1, notificationIntent,
                                PendingIntent.FLAG_CANCEL_CURRENT);

                        Resources res = ServiceQueryToServer.this.getResources();
                        Notification.Builder builder = new Notification.Builder(ServiceQueryToServer.this);

                        builder.setContentIntent(contentIntent)
                                .setSmallIcon(R.drawable.ic_mail_24dp)
                                // большая картинка
                                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_insert_drive_file_24dp))
                                //.setTicker(res.getString(R.string.warning)) // текст в строке состояния
                                .setTicker("Появились новые накладные!")
                                // .setWhen(System.currentTimeMillis())
                                .setAutoCancel(true)
                                .setLights(Color.RED, 0, 1)
                                //.setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
                                .setContentTitle("Уведомление")
                                //.setContentText(res.getString(R.string.notifytext))
                                .setContentText("В маршрутный лист добавлены новые накладные"); // Текст уведомления

                        // Notification notification = builder.getNotification(); // до API 16
                        Notification notification = builder.build();
                        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
                        notification.flags = notification.flags | Notification.FLAG_INSISTENT | Notification.FLAG_SHOW_LIGHTS;
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(2, notification);

                    }

                    //отправка уведомления о прочтении сообщений курьером
                    if (isOnline() == true) {
                        SetAnsweredSMSToServer();
                    }

                    //получение новых сообщений для курьера с сервера
                    if (isOnline() == true) {
                        String answer1C = GetNewMessagesFromServer();
                    }
                    DatabaseHelper sqlHelper2 = new DatabaseHelper(ServiceQueryToServer.this);
                    SQLiteDatabase db2 = sqlHelper2.getWritableDatabase();
                    Cursor userCursorMes = db2.rawQuery("select * from Messages where IsNew = 1 and Curier_id = " + MainActivity.TekCurier_id, null);
                    if (userCursorMes.getCount() > 0) {
                        //оповещаем о приходе новых сообщений
                        Intent notificationIntent = new Intent(ServiceQueryToServer.this, Messages_ListActivity.class);
                        notificationIntent.putExtra("IsNew", 1);
                        PendingIntent contentIntent = PendingIntent.getActivity(ServiceQueryToServer.this,
                                0, notificationIntent,
                                PendingIntent.FLAG_CANCEL_CURRENT);

                        Resources res = ServiceQueryToServer.this.getResources();
                        Notification.Builder builder = new Notification.Builder(ServiceQueryToServer.this);

                        builder.setContentIntent(contentIntent)
                                .setSmallIcon(R.drawable.ic_mail_24dp)
                                // большая картинка
                                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_mail_24dp))
                                //.setTicker(res.getString(R.string.warning)) // текст в строке состояния
                                .setTicker("Для Вас есть новые сообщения!")
                                // .setWhen(System.currentTimeMillis())
                                .setAutoCancel(true)
                                .setLights(Color.RED, 0, 1)
                                //.setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
                                .setContentTitle("Уведомление")
                                //.setContentText(res.getString(R.string.notifytext))
                                .setContentText("Появились новые сообщения из офиса"); // Текст уведомления

                        // Notification notification = builder.getNotification(); // до API 16
                        Notification notification = builder.build();
                        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
                        notification.flags = notification.flags | Notification.FLAG_INSISTENT | Notification.FLAG_SHOW_LIGHTS;
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(3, notification);

                    }

                    //отправка в 1с новой сортировки в маршрутном листе
                    if (isOnline() == true && MainActivity.isSortDocs == false ) {
                        SetTripNumberToServer();
                    }

                }

            }
        }).start();

    }

    protected boolean isOnline() {
        String cs = ServiceQueryToServer.this.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                ServiceQueryToServer.this.getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    private String GetNewDocsFromServer() {

        long TekMaxDate = 0;
        String result = "";
        String answer_res = "0"; //статус ответа 0 - ничего не получилось, FD - вернули ФД пробитого чека, -1 - создан в 1с чек, ожидающий пробития
        //ищем у курьера дату последнего загруженного события
        DatabaseHelper sqlHelper = new DatabaseHelper(this);
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        //Cursor userCursorCur = db.rawQuery("select _id, LastDocTime from curiers where _id = " + TekCurier_id, null);
        Cursor userCursorCur = db.rawQuery("select MLDate, EventDateTime from Docs where MLClose = 0 and Curier_id = "+ MainActivity.TekCurier_id+" order by MLDate, EventDateTime", null);
        if (userCursorCur.getCount() > 0) {
            userCursorCur.moveToLast();
            TekMaxDate = userCursorCur.getLong(1);
        }


        do {
            SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME_NEWDOCS);
            request.addProperty("Kod", LoginFragment.tekLogin);
            request.addProperty("FromDate", TekMaxDate);

            MarshalBase64 mbase = new MarshalBase64();
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            envelope.implicitTypes = true;
            HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
            androidHttpTransport.debug = true;

            try {
                mbase.register(envelope);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                SoapObject resultCheck = (SoapObject) envelope.getResponse();
                result = resultCheck.toString();
                PropertyInfo pi = new PropertyInfo();
                int count = resultCheck.getPropertyCount();
                for (int i = 0; i < count; i++) {
                    resultCheck.getPropertyInfo(i, pi);
                    if (pi.getName().equals("ErrorName")) {
                        result = resultCheck.getProperty(i).toString();
                        // if (result.contains("Создан новый чек ККМ")) {answer_res="-1";}
                        return result;
                    }
                }
                //нет ошибки, значит есть новые документы, загрузим их в базу данных с признаком IsNew
                MainActivity.loadDocs(resultCheck, true, getApplicationContext());
                return result;

            } catch (Exception e) {
                e.printStackTrace();
                result = e.toString();
            }
        }
        while (false);
        return result;
    }

    private String GetNewMessagesFromServer() {

        String result = "";
        DatabaseHelper sqlHelper = new DatabaseHelper(this);
        SQLiteDatabase db = sqlHelper.getWritableDatabase();

        do {
            SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME_NEWMESSAGES);
            request.addProperty("Kod", LoginFragment.tekLogin);

            MarshalBase64 mbase = new MarshalBase64();
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            envelope.implicitTypes = true;
            HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
            androidHttpTransport.debug = true;

            try {
                mbase.register(envelope);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                SoapObject resultCheck = (SoapObject) envelope.getResponse();
                result = resultCheck.toString();
                PropertyInfo pi = new PropertyInfo();
                int count = resultCheck.getPropertyCount();
                for (int i = 0; i < count; i++) {
                    resultCheck.getPropertyInfo(i, pi);
                    if (pi.getName().equals("ErrorName")) {
                        result = resultCheck.getProperty(i).toString();
                        // if (result.contains("Создан новый чек ККМ")) {answer_res="-1";}
                        return result;
                    }
                }
                //нет ошибки, значит есть новые сообщения, загрузим их в базу данных с признаком IsNew
                MainActivity.loadMessages(resultCheck, getApplicationContext());
                return result;

            } catch (Exception e) {
                e.printStackTrace();
                result = e.toString();
            }
        }
        while (false);
        return result;
    }

    private void SetAnsweredSMSToServer() {

        String result = "";

        DatabaseHelper sqlHelper3 = new DatabaseHelper(ServiceQueryToServer.this);
        SQLiteDatabase db3 = sqlHelper3.getWritableDatabase();
        Cursor userCursorMesAnsw = db3.rawQuery("select * from Messages where IsNew = 0 and IsAnswered != 1 and Curier_id = " + MainActivity.TekCurier_id, null);
        if (userCursorMesAnsw.getCount() > 0) {

            do {
                String IdSMS = "";
                for (int i = 0; i<userCursorMesAnsw.getCount(); i++)
                {
                    userCursorMesAnsw.moveToPosition(i);
                    if (IdSMS.isEmpty())
                        IdSMS = String.valueOf(userCursorMesAnsw.getInt(userCursorMesAnsw.getColumnIndex("MessageID")));
                    else
                        IdSMS = IdSMS+", "+String.valueOf(userCursorMesAnsw.getInt(userCursorMesAnsw.getColumnIndex("MessageID")));
                }
                SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME_ANSWEREDMESSAGES);
                request.addProperty("Kod", LoginFragment.tekLogin);
                request.addProperty("IDSMS", IdSMS);

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultCheck = (SoapObject) envelope.getResponse();
                    result = resultCheck.toString();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultCheck.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultCheck.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName")) {
                            result = resultCheck.getProperty(i).toString();
                            // if (result.contains("Создан новый чек ККМ")) {answer_res="-1";}
                            return;
                        }
                    }
                    //нет ошибки, значит Передали признак, запишем их в базу данных с признаком IsAnswered
                    MainActivity.loadAnsweredMessages(resultCheck, getApplicationContext());
                    return;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            while (false);
        }
    }

    private void SetTripNumberToServer() {

        String result = "";
        DatabaseHelper sqlHelper4 = new DatabaseHelper(ServiceQueryToServer.this);
        SQLiteDatabase db4 = sqlHelper4.getWritableDatabase();
        //отсылаем новую сортировку маршрута только по последнему незакрытому маршрутному листу
        Cursor userCursorML = db4.rawQuery("select MLNumber from Docs where TripNumberNew = 1 and MLClose !=1 and Curier_id = " + MainActivity.TekCurier_id+" group by MLNumber order by MLDate DESC", null);
        if (userCursorML.getCount()== 0) return;
        userCursorML.moveToFirst();
        String ML_N = userCursorML.getString(userCursorML.getColumnIndex("MLNumber"));
        Cursor userCursorDocs = db4.rawQuery("select * from Docs where MLNumber = ? and Curier_id = " + MainActivity.TekCurier_id, new String[]{ML_N});
        JSONObject Docs = new JSONObject();
        if (userCursorDocs.getCount() > 0) {
            do {
                    for (int i = 0; i < userCursorDocs.getCount(); i++) {
                    userCursorDocs.moveToPosition(i);
                    JSONObject Doc = new JSONObject();
                    try {
                        Doc.put("MLNumber", userCursorDocs.getString(userCursorDocs.getColumnIndex("MLNumber")));
                        Doc.put("MLDate", userCursorDocs.getString(userCursorDocs.getColumnIndex("MLDate")));
                        Doc.put("DocNumber", userCursorDocs.getString(userCursorDocs.getColumnIndex("DocNumber")));
                        Doc.put("DocDate", userCursorDocs.getString(userCursorDocs.getColumnIndex("MLDate")));
                        Doc.put("DocZNumber", userCursorDocs.getString(userCursorDocs.getColumnIndex("DocZNumber")));
                        Doc.put("DocType", userCursorDocs.getString(userCursorDocs.getColumnIndex("DocIsDelivery")));
                        Doc.put("TripNumber", userCursorDocs.getString(userCursorDocs.getColumnIndex("TripNumber")));
                        Docs.put("Doc_"+String.valueOf(i), Doc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                db4.close();
                SoapObject request = new SoapObject(MainActivity.NAMESPACE, METHOD_NAME_SETTRIPNUMBER);
                request.addProperty("Kod", LoginFragment.tekLogin);
                request.addProperty("ListOfDocs", Docs.toString());

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(MainActivity.URL, MainActivity.user, MainActivity.pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultCheck = (SoapObject) envelope.getResponse();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultCheck.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultCheck.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName")) {
                            result = resultCheck.getProperty(i).toString();
                            if (!result.contains("Ошибка:")) {
                                //если результат запроса не содержит ошибки, значит в МЛ записался новый порядок
                                //и можно в нашей базе установить признак этого
                                db4 = sqlHelper4.getWritableDatabase();
                                for (i = 0; i < userCursorDocs.getCount(); i++) {
                                    userCursorDocs.moveToPosition(i);
                                    String sql = "update Docs set TripNumberNew = 0 where MLNumber = ? and DocNumber = ? and DocZNumber = ?";
                                    db4.execSQL(sql, new String[]{userCursorDocs.getString(userCursorDocs.getColumnIndex("MLNumber")), userCursorDocs.getString(userCursorDocs.getColumnIndex("DocNumber")), userCursorDocs.getString(userCursorDocs.getColumnIndex("DocZNumber"))});
                                }
                                db4.close();
                            }
                            return;
                        }
                    }
                    return;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            while (false);
        }
    }
}
