package ru.newpartner.forcuriers;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ru.newpartner.forcuriers.R;

import static ru.newpartner.forcuriers.LoginFragment.tekLogin;
import static ru.newpartner.forcuriers.MainActivity.NAMESPACE;
import static ru.newpartner.forcuriers.MainActivity.TekCurier_FIO;
import static ru.newpartner.forcuriers.MainActivity.URL;
import static ru.newpartner.forcuriers.MainActivity.loadAnsweredEvents;
import static ru.newpartner.forcuriers.MainActivity.pass;
import static ru.newpartner.forcuriers.MainActivity.user;

public class Docs_Set_ActionActivity extends ActionBarActivity {

    String tekDoc_Number;
    String tekDoc_ZNumber;
    int tekDoc_isDelivery;
    Calendar cal;
    SimpleDateFormat df;
    Button btnSetAction;
    TextView txtDateTime;
    TextView txtInfo;
    public static final String SOAP_ACTION = "http://delivery-russia.ru#MobileCouriersExchange:SetDD";
    public static final String METHOD_NAME_SETACTION = "SetDD";
    private final int IDD_THREE_BUTTONS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs_set_action);
        findViewById(R.id.layout).requestFocus();
//
//        sqlHelper = new DatabaseHelper(getApplicationContext());
//        // создаем базу данных
//        sqlHelper.create_db();

        Intent intentDoc = getIntent();
        tekDoc_Number = intentDoc.getStringExtra("DocNumber");
        tekDoc_ZNumber = intentDoc.getStringExtra("DocZNumber");
        tekDoc_isDelivery = intentDoc.getIntExtra("DocIsDelivery", 0);

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        if (TekCurier_FIO != null)
            mTxtFIO.setText(TekCurier_FIO.toString());
        else
            mTxtFIO.setText("Курьер не выбран");

        TextView mTxtDocNumber = (TextView) findViewById(R.id.textViewDocNumber);
        if (tekDoc_Number != null && !tekDoc_Number.trim().isEmpty()) {
            mTxtDocNumber.setVisibility(View.VISIBLE);
            mTxtDocNumber.setText(tekDoc_Number);
        }
        txtInfo = (TextView) findViewById(R.id.textInfo);

        cal = Calendar.getInstance();
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        txtDateTime = (TextView) findViewById(R.id.editTextDate);
        txtDateTime.setText(df.format(cal.getTime()));

        //
        TimePicker tp = (TimePicker) findViewById(R.id.timePicker);
        tp.setIs24HourView(true);
        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener(){

            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                cal.set(Calendar.MINUTE, minute);
                txtDateTime.setText(df.format(cal.getTime()));
            }

            });

        //видимость ФИО и должности только для накладной, при передаче ПОДа. Для забора они не нужны
        TextInputLayout lFIO = (TextInputLayout) findViewById(R.id.LayoutFIO);
        TextInputLayout lPosition = (TextInputLayout) findViewById(R.id.LayoutPosition);
        if (tekDoc_isDelivery == 1) {
            lFIO.setVisibility(View.VISIBLE);
            lPosition.setVisibility(View.VISIBLE);
        }
        else {
            lFIO.setVisibility(View.GONE);
            lPosition.setVisibility(View.GONE);
        }

        btnSetAction = (Button) findViewById(R.id.SetEventBtn);
        if (tekDoc_isDelivery == 1)
            btnSetAction.setText("Отправить ПОД в 1С");
        else
            btnSetAction.setText("Отправить в 1С подтверждение");

        btnSetAction.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //зададим сначала вопрос о передаче ПОДа
               // showDialog(IDD_THREE_BUTTONS);
                SetAction();
            }
        })
        );
    }

    private void SetAction() {

        EditText TekFIO = (EditText) findViewById(R.id.editTextFIO);
        if (TekFIO.getText().toString().isEmpty() && tekDoc_isDelivery == 1) {
            txtInfo.setVisibility(View.VISIBLE);
            txtInfo.setText("Укажите ФИО ! ПОД не передан !");
        }
        else {
            SendAction SendTask = new SendAction();
            txtInfo.setVisibility(View.VISIBLE);
            txtInfo.setText("Попытка передачи в 1С...");
            SendTask.execute();
        }
    }

    //объявдение класса для отсылки запроса к веб-сервису
    private class SendAction extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content;
            if (isOnline() == true) {
                //запрос к веб-сервису для проверки логина и пароля
                content = SendActionToServer();
                return content;
            } else {
                content = "Ошибка: Нет доступа к интернету, ПОД не может быть передан!";
                return content;
            }
        }

        @Override
        protected void onPostExecute(String content) {

            Toast.makeText(getApplicationContext(), content, Toast.LENGTH_LONG)
                    .show();
            if (content.contains("ПОД введен"))
            {
                if (tekDoc_isDelivery == 1) {
                    txtInfo.setVisibility(View.VISIBLE);
                    txtInfo.setText("ПОД передан в 1С");
                }
                else {
                    txtInfo.setVisibility(View.VISIBLE);
                    txtInfo.setText("Подтверждение о приеме передано в 1С");
                }
                finish();
            }
            else if (content.contains("Ошибка"))
            {
                txtInfo.setVisibility(View.VISIBLE);
                txtInfo.setText(content);
            }
        }

        private String SendActionToServer()  {

            String result = "";
            do {
                JSONObject Docs = new JSONObject();
                try {
                    Docs.put("NUMBER",tekDoc_Number);
                    Docs.put("ZNUMBER",tekDoc_ZNumber);
                    Docs.put("DATE",df.format(cal.getTime()));
                    if (tekDoc_isDelivery == 1) {
                        //это накладная
                        EditText TekFIO = (EditText) findViewById(R.id.editTextFIO);
                        EditText TekPosition = (EditText) findViewById(R.id.editTextPosition);
                        Docs.put("DESCRIPTION", TekFIO.getText().toString() + "  " + TekPosition.getText().toString());
                        Docs.put("EVENT", "Доставлено");
                    }
                    else if (tekDoc_isDelivery == 2) {
                        //это вызов курьера
                        Docs.put("DESCRIPTION", "");
                        Docs.put("EVENT", "Принято вызов");
                    }
                    else {
                        //это заявка
                        Docs.put("DESCRIPTION", "");
                        Docs.put("EVENT", "Принято");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String StrReques = Docs.toString();
                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SETACTION);
                request.addProperty("Kod",tekLogin);
                request.addProperty("ListOfDocs",StrReques);

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(URL, user, pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultCheck = (SoapObject) envelope.getResponse();
                    result = resultCheck.toString();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultCheck.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultCheck.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName")) {
                            result = resultCheck.getProperty(i).toString();
                            return result;
                        }
                    }
                    //нет ошибки, значит Передали признак, запишем их в базу данных с признаком IsAnswered
                    loadAnsweredEvents(resultCheck, getApplicationContext());
                    return "ПОД введен";

                } catch (Exception e) {
                    e.printStackTrace();
                    return "Ошибка передачи по данной накладной, обратитесь к системному администратору!";
                }
            }
            while (false);
          }

    }

    protected boolean isOnline() {
        String cs = this.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                this.getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case IDD_THREE_BUTTONS:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                String msg = "";
                if (tekDoc_isDelivery == 1)
                    msg = "Передать в 1С подтверждение о доставке?";
                else
                    msg = "Передать в 1С подтверждение приеме заявки?";
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("ДА",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        SetAction();
                                    }
                                })
                        .setNegativeButton("НЕТ",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                return builder.create();
            default:
                return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
