package ru.newpartner.forcuriers;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

//import zj.com.cn.bluetooth.sdk.BluetoothService;
//import zj.com.cn.bluetooth.sdk.R1;
import ru.newpartner.forcuriers.R;
import zj.com.command.sdk.Command;
import zj.com.command.sdk.PrintPicture;
import zj.com.command.sdk.PrinterCommand;
import zj.com.customize.sdk.Other;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class PrintActivityNew extends Activity implements OnClickListener{
    /******************************************************************************************************/
    // Debugging
    private static final String TAG = "PrintActivityNew";
    private static final boolean DEBUG = true;
    /******************************************************************************************************/
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    /*******************************************************************************************************/
    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CHOSE_BMP = 3;
    private static final int REQUEST_CAMER = 4;

    //QRcode
    private static final int QR_WIDTH = 350;
    private static final int QR_HEIGHT = 350;
    /*******************************************************************************************************/
    private static final String CHINESE = "GBK";
    private static final String THAI = "CP874";
    private static final String KOREAN = "EUC-KR";
    private static final String BIG5 = "BIG5";

    /*********************************************************************************/
    private TextView mTitle;
    EditText editText;
    ImageView imageViewPicture;
    private static boolean is58mm = true;
    private RadioButton width_58mm, width_80;
    private RadioButton thai, big5, Simplified, Korean;
    private CheckBox hexBox;
    private Button sendButton = null;
    private Button testButton = null;
    private Button printbmpButton = null;
    private Button btnScanButton = null;
    private Button btnClose = null;
    private Button btn_BMP = null;
    private Button btn_ChoseCommand = null;
    private Button btn_prtsma = null;
    private Button btn_prttableButton = null;
    private Button btn_prtcodeButton = null;
    private Button btn_scqrcode = null;
    private Button btn_camer = null;

    /******************************************************************************************************/
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    private BluetoothService mService = null;

    /***************************   жЊ‡                 д»¤****************************************************************/
    final String[] items = { "е¤ЌдЅЌж‰“еЌ°жњє", "ж‰“еЌ°е№¶иµ°зєё", "ж ‡е‡†ASCIIе­—дЅ“", "еЋ‹зј©ASCIIе­—дЅ“", "ж­Јеёёе¤§е°Џ",
            "дєЊеЂЌй«еЂЌе®Ѕ", "дё‰еЂЌй«еЂЌе®Ѕ", "е››еЂЌй«еЂЌе®Ѕ", "еЏ–ж¶€еЉ зІ—жЁЎејЏ", "йЂ‰ж‹©еЉ зІ—жЁЎејЏ", "еЏ–ж¶€еЂ’зЅ®ж‰“еЌ°", "йЂ‰ж‹©еЂ’зЅ®ж‰“еЌ°", "еЏ–ж¶€й»‘з™ЅеЏЌжѕ", "йЂ‰ж‹©й»‘з™ЅеЏЌжѕ",
            "еЏ–ж¶€йЎєж—¶й’€ж—‹иЅ¬90В°", "йЂ‰ж‹©йЎєж—¶й’€ж—‹иЅ¬90В°", "иµ°зєёе€°е€‡е€ЂдЅЌзЅ®е№¶е€‡зєё", "ињ‚йёЈжЊ‡д»¤", "ж ‡е‡†й’±з®±жЊ‡д»¤",
            "е®ћж—¶еј№й’±з®±жЊ‡д»¤", "иї›е…Ґе­—з¬¦жЁЎејЏ", "иї›е…Ґдё­ж–‡жЁЎејЏ", "ж‰“еЌ°и‡ЄжЈЂйЎµ", "з¦Ѓж­ўжЊ‰й”®", "еЏ–ж¶€з¦Ѓж­ўжЊ‰й”®" ,
            "и®ѕзЅ®ж±‰е­—е­—з¬¦дё‹е€’зєї", "еЏ–ж¶€ж±‰е­—е­—з¬¦дё‹е€’зєї", "иї›е…ҐеЌЃе…­иї›е€¶жЁЎејЏ" };
    final String[] itemsen = { "Print Init", "Print and Paper", "Standard ASCII font", "Compressed ASCII font", "Normal size",
            "Double high power wide", "Twice as high power wide", "Three times the high-powered wide", "Off emphasized mode", "Choose bold mode", "Cancel inverted Print", "Invert selection Print", "Cancel black and white reverse display", "Choose black and white reverse display",
            "Cancel rotated clockwise 90 В°", "Select the clockwise rotation of 90 В°", "Feed paper Cut", "Beep", "Standard CashBox",
            "Open CashBox", "Char Mode", "Chinese Mode", "Print SelfTest", "DisEnable Button", "Enable Button" ,
            "Set Underline", "Cancel Underline", "Hex Mode" };
    final byte[][] byteCommands = {
            { 0x1b, 0x40, 0x0a },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x0a }, //ж‰“еЌ°е№¶иµ°зєё
            { 0x1b, 0x4d, 0x00 },// ж ‡е‡†ASCIIе­—дЅ“
            { 0x1b, 0x4d, 0x01 },// еЋ‹зј©ASCIIе­—дЅ“
            { 0x1d, 0x21, 0x00 },// е­—дЅ“дёЌж”ѕе¤§
            { 0x1d, 0x21, 0x11 },// е®Ѕй«еЉ еЂЌ
            { 0x1d, 0x21, 0x22 },// е®Ѕй«еЉ еЂЌ
            { 0x1d, 0x21, 0x33 },// е®Ѕй«еЉ еЂЌ
            { 0x1b, 0x45, 0x00 },// еЏ–ж¶€еЉ зІ—жЁЎејЏ
            { 0x1b, 0x45, 0x01 },// йЂ‰ж‹©еЉ зІ—жЁЎејЏ
            { 0x1b, 0x7b, 0x00 },// еЏ–ж¶€еЂ’зЅ®ж‰“еЌ°
            { 0x1b, 0x7b, 0x01 },// йЂ‰ж‹©еЂ’зЅ®ж‰“еЌ°
            { 0x1d, 0x42, 0x00 },// еЏ–ж¶€й»‘з™ЅеЏЌжѕ
            { 0x1d, 0x42, 0x01 },// йЂ‰ж‹©й»‘з™ЅеЏЌжѕ
            { 0x1b, 0x56, 0x00 },// еЏ–ж¶€йЎєж—¶й’€ж—‹иЅ¬90В°
            { 0x1b, 0x56, 0x01 },// йЂ‰ж‹©йЎєж—¶й’€ж—‹иЅ¬90В°
            { 0x0a, 0x1d, 0x56, 0x42, 0x01, 0x0a },//е€‡е€ЂжЊ‡д»¤
            { 0x1b, 0x42, 0x03, 0x03 },//ињ‚йёЈжЊ‡д»¤
            { 0x1b, 0x70, 0x00, 0x50, 0x50 },//й’±з®±жЊ‡д»¤
            { 0x10, 0x14, 0x00, 0x05, 0x05 },//е®ћж—¶еј№й’±з®±жЊ‡д»¤
            { 0x1c, 0x2e },// иї›е…Ґе­—з¬¦жЁЎејЏ
            { 0x1c, 0x26 }, //иї›е…Ґдё­ж–‡жЁЎејЏ
            { 0x1f, 0x11, 0x04 }, //ж‰“еЌ°и‡ЄжЈЂйЎµ
            { 0x1b, 0x63, 0x35, 0x01 }, //з¦Ѓж­ўжЊ‰й”®
            { 0x1b, 0x63, 0x35, 0x00 }, //еЏ–ж¶€з¦Ѓж­ўжЊ‰й”®
            { 0x1b, 0x2d, 0x02, 0x1c, 0x2d, 0x02 }, //и®ѕзЅ®дё‹е€’зєї
            { 0x1b, 0x2d, 0x00, 0x1c, 0x2d, 0x00 }, //еЏ–ж¶€дё‹е€’зєї
            { 0x1f, 0x11, 0x03 }, //ж‰“еЌ°жњєиї›е…Ґ16иї›е€¶жЁЎејЏ
    };
    /***************************жќЎ                          з Ѓ***************************************************************/
    final String[] codebar = { "UPC_A", "UPC_E", "JAN13(EAN13)", "JAN8(EAN8)",
            "CODE39", "ITF", "CODABAR", "CODE93", "CODE128", "QR Code" };
    final byte[][] byteCodebar = {
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
            { 0x1b, 0x40 },// е¤ЌдЅЌж‰“еЌ°жњє
    };
    /******************************************************************************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DEBUG)
            Log.e(TAG, "+++ ON CREATE 000 +++");

        // Set up the window layout
       // requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main_activity);
        Intent intentDoc = getIntent();
        String tekAddress = intentDoc.getStringExtra("Address");
        EditText StrToPrint = (EditText) findViewById(R.id.edit_text_out);
        StrToPrint.setText(tekAddress);
      // getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
        //        R.layout.custom_title);

        if (DEBUG)
            Log.e(TAG, "+++ ON CREATE 111 +++");

//        // Set up the custom title
       // mTitle = (TextView) findViewById(R.id.title_left_text);
      //  mTitle.setText(R.string.app_title);
      //  mTitle.setText("app_title");
      //  mTitle = (TextView) findViewById(R.id.title_right_text);

        if (DEBUG)
            Log.e(TAG, "+++ ON CREATE 222 +++");

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (DEBUG)
            Log.e(TAG, "+++ ON CREATE 333 +++");

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (mService == null)
                KeyListenerInit();//з›‘еђ¬
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        if (mService != null) {

            if (mService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService.start();
            }
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (DEBUG)
            Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if (DEBUG)
            Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (mService != null)
            mService.stop();
        if (DEBUG)
            Log.e(TAG, "--- ON DESTROY ---");
    }

    /*****************************************************************************************************/
    private void KeyListenerInit() {

        editText = (EditText) findViewById(R.id.edit_text_out);

        sendButton = (Button) findViewById(R.id.Send_Button);
        sendButton.setOnClickListener(this);

        testButton = (Button) findViewById(R.id.btn_test);
        testButton.setOnClickListener(this);

        printbmpButton = (Button) findViewById(R.id.btn_printpicture);
        printbmpButton.setOnClickListener(this);

        btnScanButton = (Button)findViewById(R.id.button_scan);
        btnScanButton.setOnClickListener(this);

        hexBox = (CheckBox)findViewById(R.id.checkBoxHEX);
        hexBox.setOnClickListener(this);

        width_58mm = (RadioButton)findViewById(R.id.width_58mm);
        width_58mm.setOnClickListener(this);

        width_80 = (RadioButton)findViewById(R.id.width_80mm);
        width_80.setOnClickListener(this);

        imageViewPicture = (ImageView) findViewById(R.id.imageViewPictureUSB);
        imageViewPicture.setOnClickListener(this);

        btnClose = (Button)findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

        btn_BMP = (Button)findViewById(R.id.btn_prtbmp);
        btn_BMP.setOnClickListener(this);

        btn_ChoseCommand = (Button)findViewById(R.id.btn_prtcommand);
        btn_ChoseCommand.setOnClickListener(this);

        btn_prtsma = (Button)findViewById(R.id.btn_prtsma);
        btn_prtsma.setOnClickListener(this);

        btn_prttableButton = (Button)findViewById(R.id.btn_prttable);
        btn_prttableButton.setOnClickListener(this);

        btn_prtcodeButton = (Button)findViewById(R.id.btn_prtbarcode);
        btn_prtcodeButton.setOnClickListener(this);

        btn_camer = (Button)findViewById(R.id.btn_dyca);
        btn_camer.setOnClickListener(this);

        btn_scqrcode = (Button)findViewById(R.id.btn_scqr);
        btn_scqrcode.setOnClickListener(this);

        Simplified = (RadioButton)findViewById(R.id.gbk12);
        Simplified.setOnClickListener(this);
        big5 = (RadioButton)findViewById(R.id.big5);
        big5.setOnClickListener(this);
        thai = (RadioButton)findViewById(R.id.thai);
        thai.setOnClickListener(this);
        Korean = (RadioButton)findViewById(R.id.kor);
        Korean.setOnClickListener(this);

        Bitmap bm = getImageFromAssetsFile("demo.bmp");
        if (null != bm) {
            imageViewPicture.setImageBitmap(bm);
        }

        editText.setEnabled(false);
        imageViewPicture.setEnabled(false);
        width_58mm.setEnabled(false);
        width_80.setEnabled(false);
        hexBox.setEnabled(false);
        sendButton.setEnabled(false);
        testButton.setEnabled(false);
        printbmpButton.setEnabled(false);
        btnClose.setEnabled(false);
        btn_BMP.setEnabled(false);
        btn_ChoseCommand.setEnabled(false);
        btn_prtcodeButton.setEnabled(false);
        btn_prtsma.setEnabled(false);
        btn_prttableButton.setEnabled(false);
        btn_camer.setEnabled(false);
        btn_scqrcode.setEnabled(false);
        Simplified.setEnabled(false);
        Korean.setEnabled(false);
        big5.setEnabled(false);
        thai.setEnabled(false);

        mService = new BluetoothService(this, mHandler);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.button_scan:{
                Intent serverIntent = new Intent(PrintActivityNew.this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                break;
            }
            case R.id.btn_close:{
                mService.stop();
                editText.setEnabled(false);
                imageViewPicture.setEnabled(false);
                width_58mm.setEnabled(false);
                width_80.setEnabled(false);
                hexBox.setEnabled(false);
                sendButton.setEnabled(false);
                testButton.setEnabled(false);
                printbmpButton.setEnabled(false);
                btnClose.setEnabled(false);
                btn_BMP.setEnabled(false);
                btn_ChoseCommand.setEnabled(false);
                btn_prtcodeButton.setEnabled(false);
                btn_prtsma.setEnabled(false);
                btn_prttableButton.setEnabled(false);
                btn_camer.setEnabled(false);
                btn_scqrcode.setEnabled(false);
                btnScanButton.setEnabled(true);
                Simplified.setEnabled(false);
                Korean.setEnabled(false);
                big5.setEnabled(false);
                thai.setEnabled(false);
                btnScanButton.setText(getText(R.string.connect));
                break;
            }
            case R.id.btn_test:{
                BluetoothPrintTest();;
                break;
            }
            case R.id.Send_Button:{
                if (hexBox.isChecked()) {
                    String str = editText.getText().toString().trim();//еЋ»жЋ‰е¤ґе°ѕз©єз™Ѕ
                    if(str.length() > 0){
                        str = Other.RemoveChar(str, ' ').toString();
                        if (str.length() <= 0)
                            return;
                        if ((str.length() % 2) != 0) {
                            Toast.makeText(getApplicationContext(), getString(R.string.msg_state),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        byte[] buf = Other.HexStringToBytes(str);
                        SendDataByte(buf);
                    }else{
                        Toast.makeText(PrintActivityNew.this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String msg = editText.getText().toString();
                    if(msg.length()>0){
                        if(thai.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, THAI, 255, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(big5.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, BIG5, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(Korean.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, KOREAN, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }else if(Simplified.isChecked()){
                            SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 0, 0, 0));
                            SendDataByte(Command.LF);
                        }
                    }else{
                        Toast.makeText(PrintActivityNew.this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
            case R.id.width_58mm:
            case R.id.width_80mm:{
                is58mm = v == width_58mm;
                width_58mm.setChecked(is58mm);
                width_80.setChecked(!is58mm);
                break;
            }
            case R.id.btn_printpicture:{
                GraphicalPrint();
                break;
            }
            case R.id.imageViewPictureUSB:{
                Intent loadpicture = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(loadpicture, REQUEST_CHOSE_BMP);
                break;
            }
            case R.id.btn_prtbmp:{
                Print_BMP();
                break;
            }
            case R.id.btn_prtcommand:{
                CommandTest();
                break;
            }
            case R.id.btn_prtsma:{
                SendDataByte(Command.ESC_Init);
                SendDataByte(Command.LF);
                Print_Ex();
                break;
            }
            case R.id.btn_prttable:{
                SendDataByte(Command.ESC_Init);
                SendDataByte(Command.LF);
                PrintTable();
                break;
            }
            case R.id.btn_prtbarcode:{
                printBarCode();
                break;
            }
            case R.id.btn_scqr:{
                createImage();
                break;
            }
            case R.id.btn_dyca:{
                dispatchTakePictureIntent(REQUEST_CAMER);
                break;
            }
            default:
                break;
        }
    }

    /*****************************************************************************************************/
	/*
	 * SendDataString
	 */
    private void SendDataString(String data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        if (data.length() > 0) {
            try {
                mService.write(data.getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     *SendDataByte
     */
    private void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        mService.write(data);
    }

    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (DEBUG)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                           // mTitle.setText(R.string.title_connected_to);
                            //mTitle.append(mConnectedDeviceName);
                            btnScanButton.setText(getText(R.string.Connecting));
                            Print_Test();//
                            btnScanButton.setEnabled(false);
                            editText.setEnabled(true);
                            imageViewPicture.setEnabled(true);
                            width_58mm.setEnabled(true);
                            width_80.setEnabled(true);
                            hexBox.setEnabled(true);
                            sendButton.setEnabled(true);
                            testButton.setEnabled(true);
                            printbmpButton.setEnabled(true);
                            btnClose.setEnabled(true);
                            btn_BMP.setEnabled(true);
                            btn_ChoseCommand.setEnabled(true);
                            btn_prtcodeButton.setEnabled(true);
                            btn_prtsma.setEnabled(true);
                            btn_prttableButton.setEnabled(true);
                            btn_camer.setEnabled(true);
                            btn_scqrcode.setEnabled(true);
                            Simplified.setEnabled(true);
                            Korean.setEnabled(true);
                            big5.setEnabled(true);
                            thai.setEnabled(true);
                            break;
                        case BluetoothService.STATE_CONNECTING:
                          //  mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                          //  mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                case MESSAGE_CONNECTION_LOST:    //и“ќз‰™е·Іж–­ејЂиїћжЋҐ
                    Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    editText.setEnabled(false);
                    imageViewPicture.setEnabled(false);
                    width_58mm.setEnabled(false);
                    width_80.setEnabled(false);
                    hexBox.setEnabled(false);
                    sendButton.setEnabled(false);
                    testButton.setEnabled(false);
                    printbmpButton.setEnabled(false);
                    btnClose.setEnabled(false);
                    btn_BMP.setEnabled(false);
                    btn_ChoseCommand.setEnabled(false);
                    btn_prtcodeButton.setEnabled(false);
                    btn_prtsma.setEnabled(false);
                    btn_prttableButton.setEnabled(false);
                    btn_camer.setEnabled(false);
                    btn_scqrcode.setEnabled(false);
                    Simplified.setEnabled(false);
                    Korean.setEnabled(false);
                    big5.setEnabled(false);
                    thai.setEnabled(false);
                    break;
                case MESSAGE_UNABLE_CONNECT:     //ж— жі•иїћжЋҐи®ѕе¤‡
                    Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:{
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras().getString(
                            DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        BluetoothDevice device = mBluetoothAdapter
                                .getRemoteDevice(address);
                        // Attempt to connect to the device
                        mService.connect(device);
                    }
                }
                break;
            }
            case REQUEST_ENABLE_BT:{
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    KeyListenerInit();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            }
            case REQUEST_CHOSE_BMP:{
                if (resultCode == Activity.RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaColumns.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, opts);
                    opts.inJustDecodeBounds = false;
                    if (opts.outWidth > 1200) {
                        opts.inSampleSize = opts.outWidth / 1200;
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, opts);
                    if (null != bitmap) {
                        imageViewPicture.setImageBitmap(bitmap);
                    }
                }else{
                    Toast.makeText(this, getString(R.string.msg_statev1), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case REQUEST_CAMER:{
                if (resultCode == Activity.RESULT_OK){
                    handleSmallCameraPhoto(data);
                }else{
                    Toast.makeText(this, getText(R.string.camer), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

/****************************************************************************************************/
    /**
     * иїћжЋҐж€ђеЉџеђЋж‰“еЌ°жµ‹иЇ•йЎµ
     */
    private void Print_Test(){
        String lang = getString(R.string.strLang);
        if((lang.compareTo("en")) == 0){
//            String msg = "Congratulations!\n\n";
//            String data = "You have sucessfully created communications between your device and our bluetooth printer.\n"
//                    +"  the company is a high-tech enterprise which specializes" +
//                    " in R&D,manufacturing,marketing of thermal printers and barcode scanners.\n\n";
            String msg = "Связь с принтером установлена!\n\n";
            String data = "----\n\n";
            //SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 1, 1, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 0, 0, 48));
            SendDataByte(PrinterCommand.POS_Print_Text(data, CHINESE, 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }else if((lang.compareTo("cn")) == 0){
            String msg = "жЃ­е–њж‚Ё!\n\n";
            String data = "ж‚Ёе·Із»Џж€ђеЉџзљ„иїћжЋҐдёЉдє†ж€‘д»¬зљ„дѕїжђєејЏи“ќз‰™ж‰“еЌ°жњєпјЃ\nж€‘д»¬е…¬еЏёжЇдёЂе®¶дё“дёљд»Ћдє‹з ”еЏ‘пјЊз”џдє§пјЊй”Ђе”®е•†з”ЁзҐЁжЌ®ж‰“еЌ°жњєе’ЊжќЎз Ѓж‰«жЏЏи®ѕе¤‡дєЋдёЂдЅ“зљ„й«з§‘жЉЂдјЃдёљ.\n\n\n\n\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 1, 1, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(data, CHINESE, 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }else if((lang.compareTo("hk")) == 0 ){
            String msg = "жЃ­е–њж‚Ё!\n";
            String data = "ж‚Ёе·Із¶“ж€ђеЉџзљ„йЂЈжЋҐдёЉдє†ж€‘еЂ‘зљ„дѕїж”њејЏи—Ќз‰™ж‰“еЌ°ж©џпјЃ \nж€‘еЂ‘е…¬еЏёжЇдёЂе®¶е°€жҐ­еѕћдє‹з ”з™јпјЊз”џз”ўпјЊйЉ·е”®е•†з”ЁзҐЁж“љж‰“еЌ°ж©џе’ЊжўќзўјжЋѓжЏЏиЁ­е‚™ж–јдёЂй«”зљ„й«з§‘жЉЂдјЃжҐ­.\n\n\n\n\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, BIG5, 0, 1, 1, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(data, BIG5, 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }else if((lang.compareTo("kor")) == 0){
            String msg = "м¶•н• н•ґмљ”!\n";
            String data = "м„±кіµм ЃмњјлЎњ мљ°л¦¬мќ нњґлЊЂмљ© лё”лЈЁн€¬мЉ¤ н”„л¦°н„°м—ђ м—°кІ° н•њ! \nмљ°л¦¬лЉ” н•мќґн…ЊнЃ¬ кё°м—… м¤‘ н•л‚м—ђм„њ к°њл°њ, мѓќм‚° л°Џ мѓЃм—… мЃм€м¦ќ н”„л¦°н„°м™Ђ л°”мЅ”л“њ мЉ¤мєђл‹ќ мћҐл№„ нЊђл§¤ м „л¬ё нљЊм‚¬мћ…л‹€л‹¤.\n\n\n\n\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, KOREAN, 0, 1, 1, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(data, KOREAN, 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }else if((lang.compareTo("thai")) == 0){
            String msg = "аё‚аё­а№ЃаёЄаё”аё‡аё„аё§аёІаёЎаёўаёґаё™аё”аёµ!\n";
            String data = "аё„аёёаё“а№„аё”а№‰а№ЂаёЉаё·а№€аё­аёЎаё•а№€аё­аёЃаё±аёљаёљаёҐаё№аё—аё№ аё а№Ђаё„аёЈаё·а№€аё­аё‡аёћаёґаёЎаёћа№Ња№ЃаёљаёљаёћаёЃаёћаёІаё‚аё­аё‡а№ЂаёЈаёІ! \n а№ЂаёЈаёІа№Ђаё›а№‡аё™ аёљаёЈаёґаё©аё±аё— аё—аёµа№€аёЎаёµаё„аё§аёІаёЎа№ЂаёЉаёµа№€аёўаё§аёЉаёІаёЌа№ѓаё™аёЃаёІаёЈаёћаё±аё’аё™аёІ, аёЃаёІаёЈаёњаёҐаёґаё•а№ЃаёҐаё°аёЃаёІаёЈаё‚аёІаёўаё‚аё­аё‡а№Ђаё„аёЈаё·а№€аё­аё‡аёћаёґаёЎаёћа№Ња№ѓаёља№ЂаёЄаёЈа№‡аё€аёЈаё±аёља№Ђаё‡аёґаё™а№ЃаёҐаё°аёЃаёІаёЈаёЄа№ЃаёЃаё™аёљаёІаёЈа№Ња№‚аё„а№‰аё”аё­аёёаё›аёЃаёЈаё“а№Ња№ЂаёЉаёґаё‡аёћаёІаё“аёґаёЉаёўа№Ња№ѓаё™аё«аё™аё¶а№€аё‡а№ѓаё™аё­аё‡аё„а№ЊаёЃаёЈаё—аёµа№€аёЎаёµа№Ђаё—аё„а№‚аё™а№‚аёҐаёўаёµаёЄаё№аё‡.\n\n\n\n\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, THAI, 255, 1, 1, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(data, THAI, 255, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }
    }

    /**
     * ж‰“еЌ°жµ‹иЇ•йЎµ
    // * @param mPrinter
     */
    private void BluetoothPrintTest() {
        String msg = "";
        String lang = getString(R.string.strLang);
        if((lang.compareTo("en")) == 0 ){
            msg = "Division I is a research and development, production and services in one high-tech research and development, production-oriented enterprises, specializing in POS terminals finance, retail, restaurants, bars, songs and other areas, computer terminals, self-service terminal peripheral equipment R & D, manufacturing and sales! \n company's organizational structure concise and practical, pragmatic style of rigorous, efficient operation. Integrity, dedication, unity, and efficient is the company's corporate philosophy, and constantly strive for today, vibrant, the company will be strong scientific and technological strength, eternal spirit of entrepreneurship, the pioneering and innovative attitude, confidence towards the international information industry, with friends to create brilliant information industry !!! \n\n\n";
            SendDataString(msg);
        }else if((lang.compareTo("cn")) == 0){
            msg = "ж€‘еЏёжЇдёЂе®¶й›†з§‘з ”ејЂеЏ‘гЂЃз”џдє§з»ЏиђҐе’ЊжњЌеЉЎдєЋдёЂдЅ“зљ„й«жЉЂжњЇз ”еЏ‘гЂЃз”џдє§ећ‹дјЃдёљпјЊдё“дёљд»Ћдє‹й‡‘ићЌгЂЃе•†дёљй›¶е”®гЂЃй¤ђйҐ®гЂЃй…’еђ§гЂЃж­Њеђ§з­‰йў†еџџзљ„POSз»€з«ЇгЂЃи®Ўз®—жњєз»€з«ЇгЂЃи‡ЄеЉ©з»€з«Їе‘Ёиѕ№й…ЌеҐ—и®ѕе¤‡зљ„з ”еЏ‘гЂЃе€¶йЂ еЏЉй”Ђе”®пјЃ\nе…¬еЏёзљ„з»„з»‡жњєжћ„з®Ђз»ѓе®ћз”ЁпјЊдЅњйЈЋеЉЎе®ћдёҐи°ЁпјЊиїђиЎЊй«ж•€гЂ‚иЇљдїЎгЂЃж•¬дёљгЂЃе›ўз»“гЂЃй«ж•€жЇе…¬еЏёзљ„дјЃдёљзђ†еїµе’ЊдёЌж–­иїЅж±‚д»Ље¤©пјЊжњќж°”и“¬е‹ѓпјЊе…¬еЏёе°†д»Ґй›„еЋљзљ„з§‘жЉЂеЉ›й‡ЏпјЊж°ёжЃ’зљ„е€›дёљзІѕзҐћпјЊдёЌж–­ејЂж‹“е€›ж–°зљ„е§їжЂЃпјЊе……ж»ЎдїЎеїѓзљ„жњќзќЂе›Ѕй™…еЊ–дїЎжЃЇдє§дёљйў†еџџпјЊдёЋжњ‹еЏ‹д»¬жђєж‰‹е…±е€›дїЎжЃЇдє§дёљзљ„иѕ‰з…Њ!!!\n\n\n";
            SendDataString(msg);
        }else if((lang.compareTo("hk")) == 0){
            msg = "ж€‘еЏёжЇдёЂе®¶й›†з§‘з ”й–‹з™јгЂЃз”џз”ўз¶“з‡џе’ЊжњЌе‹™ж–јдёЂй«”зљ„й«жЉЂиЎ“з ”з™јгЂЃз”џз”ўећ‹дјЃжҐ­пјЊе°€жҐ­еѕћдє‹й‡‘ићЌгЂЃе•†жҐ­й›¶е”®гЂЃй¤ђйЈІгЂЃй…’еђ§гЂЃж­Њеђ§з­‰й еџџзљ„POSзµ‚з«ЇгЂЃиЁ€з®—ж©џзµ‚з«ЇгЂЃи‡ЄеЉ©зµ‚з«Їе‘Ёй‚Љй…ЌеҐ—иЁ­е‚™зљ„з ”з™јгЂЃиЈЅйЂ еЏЉйЉ·е”®пјЃ \nе…¬еЏёзљ„зµ„з№”ж©џж§‹з°Ўз·ґеЇ¦з”ЁпјЊдЅњйўЁе‹™еЇ¦ељґи¬№пјЊйЃ‹иЎЊй«ж•€гЂ‚иЄ дїЎгЂЃж•¬жҐ­гЂЃењзµђгЂЃй«ж•€жЇе…¬еЏёзљ„дјЃжҐ­зђ†еїµе’ЊдёЌж–·иїЅж±‚д»Ље¤©пјЊжњќж°Ји“¬е‹ѓпјЊе…¬еЏёе°‡д»Ґй›„еЋљзљ„з§‘жЉЂеЉ›й‡ЏпјЊж°ёжЃ†зљ„е‰µжҐ­зІѕзҐћпјЊдёЌж–·й–‹ж‹“е‰µж–°зљ„е§їж…‹пјЊе……ж»їдїЎеїѓзљ„жњќи‘—ењ‹йљ›еЊ–дїЎжЃЇз”ўжҐ­й еџџпјЊи€‡жњ‹еЏ‹еЂ‘ж”њж‰‹е…±е‰µдїЎжЃЇз”ўжҐ­зљ„ијќз…Њ!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, BIG5, 0, 0, 0, 0));
        }else if((lang.compareTo("kor")) == 0){
            msg = "л¶Ђл¬ё IлЉ” кё€мњµ, м†Њл§¤, л €мЉ¤н† лћ‘, л°”, л…ёлћ л°Џ кё°нѓЂ л¶„м•ј, м»ґн“Ён„° л‹Ёл§ђкё°, м…Ђн”„ м„њл№„мЉ¤ н„°лЇёл„ђ мЈјліЂ мћҐм№ POS н„°лЇёл„ђмќ„ м „л¬ёмњјлЎњ н•њ мІЁл‹Ё кё°м€  м—°кµ¬ л°Џ к°њл°њ, мѓќм‚° м§Ђн–Ґм Ѓ мќё кё°м—…мќ м—°кµ¬ л°Џ к°њл°њ, мѓќм‚° л°Џ м„њл№„мЉ¤мћ…л‹€л‹¤ R & D, м њмЎ° л°Џ нЊђл§¤! \n нљЊм‚¬мќ мЎ°м§Ѓ кµ¬мЎ°мќ к°„кІ°н•кі  м—„кІ©н•њ, нљЁмњЁм Ѓмќё мљґмЃмќ м‹¤м њ, м‹¤мљ©м Ѓмќё мЉ¤нѓЂмќј. л¬ґкІ°м„±, н—Њм‹ , л‹ЁкІ°, нљЁмњЁм Ѓмќё нљЊм‚¬мќ кё°м—… мІ н•™мќґл©°, м§Ђм†Ќм ЃмњјлЎњ, н™њкё°м°¬,мќґ нљЊм‚¬лЉ” к°•л Ґн•њ кіјн•™ кё°м€  к°•лЏ„, кё°м—…к°Ђ м •м‹ мќ мЃм›ђн•њ м •м‹ мќґ лђ  кІѓмћ…л‹€л‹¤ м¤лЉмќ„ мњ„н•ґ л…ёл Ґ, к°њмІ™кіј нЃм‹ м Ѓмќё нѓњлЏ„, кµ­м њ м •ліґ м‚°м—…мќ„ н–Ґн•ґ мћђм‹ к°ђ, м№њкµ¬м™Ђ н•Ёк» н™”л ¤н•њ м •ліґ м‚°м—…мќ„ л§Њл“¤ м€ мћ€мЉµл‹€л‹¤!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, KOREAN, 0, 0, 0, 0));
        }else if((lang.compareTo("thai")) == 0){
            msg = "аёЄа№€аё§аё™аё‰аё±аё™аё„аё·аё­аёЃаёІаёЈаё§аёґаё€аё±аёўа№ЃаёҐаё°аёЃаёІаёЈаёћаё±аё’аё™аёІаёЃаёІаёЈаёњаёҐаёґаё•а№ЃаёҐаё°аёЃаёІаёЈаёљаёЈаёґаёЃаёІаёЈа№ѓаё™аёЃаёІаёЈаё§аёґаё€аё±аёўаё«аё™аё¶а№€аё‡аё—аёµа№€аёЎаёµа№Ђаё—аё„а№‚аё™а№‚аёҐаёўаёµаёЄаё№аё‡а№ЃаёҐаё°аёЃаёІаёЈаёћаё±аё’аё™аёІаёЄаё–аёІаё™аё›аёЈаё°аёЃаё­аёљаёЃаёІаёЈаёњаёҐаёґаё•аё—аёµа№€аёЎаёёа№€аё‡а№Ђаё™а№‰аё™аё„аё§аёІаёЎа№ЂаёЉаёµа№€аёўаё§аёЉаёІаёЌа№ѓаё™аё‚аё±а№‰аё§ POS аёЃаёІаёЈа№Ђаё‡аёґаё™, аё„а№‰аёІаё›аёҐаёµаёЃ, аёЈа№‰аёІаё™аё­аёІаё«аёІаёЈ, аёљаёІаёЈа№Њ, а№ЂаёћаёҐаё‡а№ЃаёҐаё°аёћаё·а№‰аё™аё—аёµа№€аё­аё·а№€аё™ а№† , а№Ђаё„аёЈаё·а№€аё­аё‡аё„аё­аёЎаёћаёґаё§а№Ђаё•аё­аёЈа№Њ, аёљаёЈаёґаёЃаёІаёЈаё•аё™а№Ђаё­аё‡аё‚аё±а№‰аё§аё­аёёаё›аёЃаёЈаё“а№Њаё•а№€аё­аёћа№€аё§аё‡ R & D, аёЃаёІаёЈаёњаёҐаёґаё•а№ЃаёҐаё°аёўаё­аё”аё‚аёІаёў! \n аёЃаёЈаё°аёЉаё±аёља№‚аё„аёЈаё‡аёЄаёЈа№‰аёІаё‡ аёљаёЈаёґаё©аё±аё—  аё‚аё­аё‡аё­аё‡аё„а№ЊаёЃаёЈа№ЃаёҐаё°аёЃаёІаёЈаё›аёЏаёґаёљаё±аё•аёґа№ѓаё™аё—аёІаё‡аё›аёЏаёґаёљаё±аё•аёґаё‚аё­аё‡аёЄа№„аё•аёҐа№Њаё­аёўа№€аёІаё‡а№Ђаё‚а№‰аёЎаё‡аё§аё”аё”аёіа№Ђаё™аёґаё™аё‡аёІаё™аёЎаёµаё›аёЈаё°аёЄаёґаё—аёаёґаё аёІаёћ аё„аё§аёІаёЎаё‹аё·а№€аё­аёЄаё±аё•аёўа№Њаё—аёёа№€аёЎа№Ђаё—аё„аё§аёІаёЎаёЄаёІаёЎаё±аё„аё„аёµа№ЃаёҐаё°аёЎаёµаё›аёЈаё°аёЄаёґаё—аёаёґаё аёІаёћаё„аё·аё­аё›аёЈаё±аёЉаёЌаёІаё‚аё­аё‡аё­аё‡аё„а№ЊаёЃаёЈаё‚аё­аё‡ аёљаёЈаёґаё©аё±аё— аё­аёўа№€аёІаё‡аё•а№€аё­а№Ђаё™аё·а№€аё­аё‡а№ЃаёҐаё°аёЎаёёа№€аё‡аёЎаё±а№€аё™а№Ђаёћаё·а№€аё­аё§аё±аё™аё™аёµа№‰аё—аёµа№€аёЄаё”а№ѓаёЄаё‚аё­аё‡ аёљаёЈаёґаё©аё±аё— аё€аё°аёЎаёµаёЃаёіаёҐаё±аё‡а№ЃаёЈаё‡аё‚аё¶а№‰аё™аё—аёІаё‡аё§аёґаё—аёўаёІаёЁаёІаёЄаё•аёЈа№Ња№ЃаёҐаё°а№Ђаё—аё„а№‚аё™а№‚аёҐаёўаёµаё—аёµа№€а№Ѓаё‚а№‡аё‡а№ЃаёЃаёЈа№€аё‡аё€аёґаё•аё§аёґаёЌаёЌаёІаё“аё™аёґаёЈаё±аё™аё”аёЈа№Њаё‚аё­аё‡аёњаё№а№‰аё›аёЈаё°аёЃаё­аёљаёЃаёІаёЈаё—аёµа№€аёЎаёµаё—аё±аёЁаё™аё„аё•аёґаё—аёµа№€а№Ђаё›а№‡аё™аёњаё№а№‰аёљаёёаёЃа№ЂаёљаёґаёЃа№ЃаёҐаё°аё™аё§аё±аё•аёЃаёЈаёЈаёЎаё„аё§аёІаёЎа№ЂаёЉаё·а№€аё­аёЎаё±а№€аё™аё—аёµа№€аёЎаёµаё•а№€аё­аё­аёёаё•аёЄаёІаё«аёЃаёЈаёЈаёЎаё‚а№‰аё­аёЎаё№аёҐаёЈаё°аё«аё§а№€аёІаё‡аё›аёЈаё°а№Ђаё—аёЁ аёЃаё±аёља№Ђаёћаё·а№€аё­аё™ а№† а№ѓаё™аёЃаёІаёЈаёЄаёЈа№‰аёІаё‡аё­аёёаё•аёЄаёІаё«аёЃаёЈаёЈаёЎаё‚а№‰аё­аёЎаё№аёҐаё—аёµа№€аёўаё­аё”а№Ђаёўаёµа№€аёўаёЎ!!!\n\n\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, THAI, 255, 0, 0, 0));
        }
    }

    /*
     * ж‰“еЌ°е›ѕз‰‡
     */
    private void Print_BMP(){

        //	byte[] buffer = PrinterCommand.POS_Set_PrtInit();
        Bitmap mBitmap = ((BitmapDrawable) imageViewPicture.getDrawable())
                .getBitmap();
        int nMode = 0;
        int nPaperWidth = 384;
        if(width_58mm.isChecked())
            nPaperWidth = 384;
        else if (width_80.isChecked())
            nPaperWidth = 576;
        if(mBitmap != null)
        {
            /**
             * Parameters:
             * mBitmap  и¦Ѓж‰“еЌ°зљ„е›ѕз‰‡
             * nWidth   ж‰“еЌ°е®Ѕеє¦пј€58е’Њ80пј‰
             * nMode    ж‰“еЌ°жЁЎејЏ
             * Returns: byte[]
             */
            byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
            //	SendDataByte(buffer);
            SendDataByte(Command.ESC_Init);
            SendDataByte(Command.LF);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }
    }

    /**
     * ж‰“еЌ°и‡Єе®љд№‰иЎЁж ј
     */
    @SuppressLint("SimpleDateFormat")
    private void PrintTable(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyyе№ґMMжњ€ddж—Ґ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//иЋ·еЏ–еЅ“е‰Ќж—¶й—ґ
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if(is58mm){

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("в”Џв”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”“\n").getBytes("GBK"),
                            String.format("в”ѓеЏ‘з«™в”ѓ%-4sв”ѓе€°з«™в”ѓ%-6sв”ѓ\n","ж·±ењі","ж€ђйѓЅ").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓд»¶ж•°в”ѓ%2d/%-3dв”ѓеЌ•еЏ·в”ѓ%-8dв”ѓ\n",1,222,555).getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”»в”ів”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓж”¶д»¶дєєв”ѓ%-12sв”ѓ\n","гЂђйЂЃгЂ‘жµ‹иЇ•/жµ‹иЇ•дєє").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓдёљеЉЎе‘в”ѓ%-2sв”ѓеђЌз§°в”ѓ%-6sв”ѓ\n","жµ‹иЇ•","ж·±ењі").getBytes("GBK"),
                            String.format("в”—в”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”›\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO и‡ЄеЉЁз”џж€ђзљ„ catch еќ—
                    e.printStackTrace();
                }
            }else {

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("в”Џв”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”“\n").getBytes("GBK"),
                            String.format("в”ѓеЏ‘з«™в”ѓ%-12sв”ѓе€°з«™в”ѓ%-14sв”ѓ\n", "ж·±ењі", "ж€ђйѓЅ").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓд»¶ж•°в”ѓ%6d/%-7dв”ѓеЌ•еЏ·в”ѓ%-16dв”ѓ\n", 1, 222, 55555555).getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”»в”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓж”¶д»¶дєєв”ѓ%-28sв”ѓ\n", "гЂђйЂЃгЂ‘жµ‹иЇ•/жµ‹иЇ•дєє").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓдёљеЉЎе‘в”ѓ%-10sв”ѓеђЌз§°в”ѓ%-14sв”ѓ\n", "жµ‹иЇ•", "ж·±ењі").getBytes("GBK"),
                            String.format("в”—в”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”›\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO и‡ЄеЉЁз”џж€ђзљ„ catch еќ—
                    e.printStackTrace();
                }
            }
        }else if((lang.compareTo("en")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd/ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//иЋ·еЏ–еЅ“е‰Ќж—¶й—ґ
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if(is58mm){

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("в”Џв”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”“\n").getBytes("GBK"),
                            String.format("в”ѓXXXXв”ѓ%-6sв”ѓXXXXв”ѓ%-8sв”ѓ\n","XXXX","XXXX").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXв”ѓ%2d/%-3dв”ѓXXXXв”ѓ%-8dв”ѓ\n",1,222,555).getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”»в”ів”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXXXв”ѓ%-18sв”ѓ\n","гЂђXXгЂ‘XXXX/XXXXXX").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXXXв”ѓ%-2sв”ѓXXXXв”ѓ%-8sв”ѓ\n","XXXX","XXXX").getBytes("GBK"),
                            String.format("в”—в”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”›\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO и‡ЄеЉЁз”џж€ђзљ„ catch еќ—
                    e.printStackTrace();
                }
            }else {

                Command.ESC_Align[2] = 0x02;
                byte[][] allbuf;
                try {
                    allbuf = new byte[][]{

                            Command.ESC_Init, Command.ESC_Three,
                            String.format("в”Џв”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”“\n").getBytes("GBK"),
                            String.format("в”ѓXXXXв”ѓ%-14sв”ѓXXXXв”ѓ%-16sв”ѓ\n", "XXXX", "XXXX").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXв”ѓ%6d/%-7dв”ѓXXXXв”ѓ%-16dв”ѓ\n", 1, 222, 55555555).getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”»в”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXXXв”ѓ%-34sв”ѓ\n", "гЂђXXгЂ‘XXXX/XXXXXX").getBytes("GBK"),
                            String.format("в”Јв”Ѓв”Ѓв”Ѓв•‹в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”ів”Ѓв”Ѓв”ів”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”«\n").getBytes("GBK"),
                            String.format("в”ѓXXXXXXв”ѓ%-12sв”ѓXXXXв”ѓ%-16sв”ѓ\n", "XXXX", "XXXX").getBytes("GBK"),
                            String.format("в”—в”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”»в”Ѓв”Ѓв”»в”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”Ѓв”›\n").getBytes("GBK"),
                            Command.ESC_Align, "\n".getBytes("GBK")
                    };
                    byte[] buf = Other.byteArraysToBytes(allbuf);
                    SendDataByte(buf);
                    SendDataString(date);
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO и‡ЄеЉЁз”џж€ђзљ„ catch еќ—
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ж‰“еЌ°и‡Єе®љд№‰е°ЏзҐЁ
     */
    @SuppressLint("SimpleDateFormat")
    private void Print_Ex(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyyе№ґMMжњ€ddж—Ґ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//иЋ·еЏ–еЅ“е‰Ќж—¶й—ґ
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if (is58mm) {

                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("зѓ­ж•Џж‰“еЌ°жњє!", 0, 3, 6);//
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKEдё“еЌ–еє—\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("й—Ёеє—еЏ·: 888888\nеЌ•жЌ®  S00003333\nж”¶й“¶е‘пјљ1001\nеЌ•жЌ®ж—Ґжњџпјљxxxx-xx-xx\nж‰“еЌ°ж—¶й—ґпјљxxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("е“ЃеђЌ       ж•°й‡Џ    еЌ•д»·    й‡‘йўќ\nNIKEи·‘йћ‹   10.00   899     8990\nNIKEзЇ®зђѓйћ‹ 10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("ж•°й‡Џпјљ                20.00\nжЂ»и®Ўпјљ                16889.00\nд»ж¬ѕпјљ                17000.00\nж‰ѕй›¶пјљ                111.00\n".getBytes("GBK"));
                    SendDataByte("е…¬еЏёеђЌз§°пјљNIKE\nе…¬еЏёзЅ‘еќЂпјљwww.xxx.xxx\nењ°еќЂпјљж·±ењіеё‚xxеЊєxxеЏ·\nз”µиЇќпјљ0755-11111111\nжњЌеЉЎдё“зєїпјљ400-xxx-xxxx\n================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("и°ўи°ўжѓ йЎѕ,ж¬ўиїЋе†Ќж¬Ўе…‰дёґ!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);

                    SendDataByte("(д»ҐдёЉдїЎжЃЇдёєжµ‹иЇ•жЁЎжќї,е¦‚жњ‰и‹џеђЊпјЊзєЇе±ће·§еђ€!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("зѓ­ж•Џж‰“еЌ°жњє!", 0, 3, 6);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKEдё“еЌ–еє—\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("й—Ёеє—еЏ·: 888888\nеЌ•жЌ®  S00003333\nж”¶й“¶е‘пјљ1001\nеЌ•жЌ®ж—Ґжњџпјљxxxx-xx-xx\nж‰“еЌ°ж—¶й—ґпјљxxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("е“ЃеђЌ            ж•°й‡Џ    еЌ•д»·    й‡‘йўќ\nNIKEи·‘йћ‹        10.00   899     8990\nNIKEзЇ®зђѓйћ‹      10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("ж•°й‡Џпјљ                20.00\nжЂ»и®Ўпјљ                16889.00\nд»ж¬ѕпјљ                17000.00\nж‰ѕй›¶пјљ                111.00\n".getBytes("GBK"));
                    SendDataByte("е…¬еЏёеђЌз§°пјљNIKE\nе…¬еЏёзЅ‘еќЂпјљwww.xxx.xxx\nењ°еќЂпјљж·±ењіеё‚xxеЊєxxеЏ·\nз”µиЇќпјљ0755-11111111\nжњЌеЉЎдё“зєїпјљ400-xxx-xxxx\n===========================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("и°ўи°ўжѓ йЎѕ,ж¬ўиїЋе†Ќж¬Ўе…‰дёґ!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("(д»ҐдёЉдїЎжЃЇдёєжµ‹иЇ•жЁЎжќї,е¦‚жњ‰и‹џеђЊпјЊзєЇе±ће·§еђ€!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }else if((lang.compareTo("en")) == 0){
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd/ HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());//иЋ·еЏ–еЅ“е‰Ќж—¶й—ґ
            String str = formatter.format(curDate);
            String date = str + "\n\n\n\n\n\n";
            if (is58mm) {

                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 6);//
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKE Shop\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Number:  888888\nReceipt  S00003333\nCashierпјљ1001\nDateпјљxxxx-xx-xx\nPrint Timeпјљxxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Name    Quantity    price  Money\nShoes   10.00       899     8990\nBall    10.00       1599    15990\n".getBytes("GBK"));
                    SendDataByte("Quantityпјљ             20.00\ntotalпјљ                16889.00\npaymentпјљ              17000.00\nKeep the changeпјљ      111.00\n".getBytes("GBK"));
                    SendDataByte("company nameпјљNIKE\nSiteпјљwww.xxx.xxx\naddressпјљShenzhenxxAreaxxnumber\nphone numberпјљ0755-11111111\nHelplineпјљ400-xxx-xxxx\n================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Welcome again!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);

                    SendDataByte("(The above information is for testing template, if agree, is purely coincidental!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                try {
                    byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 8);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);

                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("NIKE Shop\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Number: 888888\nReceipt  S00003333\nCashierпјљ1001\nDateпјљxxxx-xx-xx\nPrint Timeпјљxxxx-xx-xx  xx:xx:xx\n".getBytes("GBK"));
                    SendDataByte("Name                    Quantity price  Money\nNIKErunning shoes        10.00   899     8990\nNIKEBasketball Shoes     10.00   1599    15990\n".getBytes("GBK"));
                    SendDataByte("Quantityпјљ               20.00\ntotalпјљ                  16889.00\npaymentпјљ                17000.00\nKeep the changeпјљ                111.00\n".getBytes("GBK"));
                    SendDataByte("company nameпјљNIKE\nSiteпјљwww.xxx.xxx\naddressпјљshenzhenxxAreaxxnumber\nphone numberпјљ0755-11111111\nHelplineпјљ400-xxx-xxxx\n================================================\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("Welcome again!\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x00;
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("(The above information is for testing template, if agree, is purely coincidental!)\n".getBytes("GBK"));
                    Command.ESC_Align[2] = 0x02;
                    SendDataByte(Command.ESC_Align);
                    SendDataString(date);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
                    SendDataByte(Command.GS_V_m_n);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ж‰“еЌ°жќЎз ЃгЂЃдєЊз»ґз Ѓ
     */
    private void printBarCode() {

        new AlertDialog.Builder(PrintActivityNew.this).setTitle(getText(R.string.btn_prtcode))
                .setItems(codebar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SendDataByte(byteCodebar[which]);
                        String str = editText.getText().toString();
                        if(which == 0)
                        {
                            if(str.length() == 11 || str.length() == 12)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 65, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("UPC_A\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 1)
                        {
                            if(str.length() == 6 || str.length() == 7)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 66, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("UPC_E\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 2)
                        {
                            if(str.length() == 12 || str.length() == 13)
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 67, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("JAN13(EAN13)\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 3)
                        {
                            if(str.length() >0 )
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 68, 3, 168, 0, 2);
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00});
                                SendDataString("JAN8(EAN8)\n");
                                SendDataByte(code);
                            }else {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else if(which == 4)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 69, 3, 168, 1, 2);
                                SendDataString("CODE39\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 5)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 70, 3, 168, 1, 2);
                                SendDataString("ITF\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 6)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 71, 3, 168, 1, 2);
                                SendDataString("CODABAR\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 7)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 72, 3, 168, 1, 2);
                                SendDataString("CODE93\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 8)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.msg_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getCodeBarCommand(str, 73, 3, 168, 1, 2);
                                SendDataString("CODE128\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                        else if(which == 9)
                        {
                            if(str.length() == 0)
                            {
                                Toast.makeText(PrintActivityNew.this, getText(R.string.empty1), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                byte[] code = PrinterCommand.getBarCommand(str, 1, 3, 8);
                                SendDataString("QR Code\n");
                                SendDataByte(new byte[]{0x1b, 0x61, 0x00 });
                                SendDataByte(code);
                            }
                        }
                    }
                }).create().show();
    }

    /**
     * public static Bitmap createAppIconText(Bitmap icon, String txt, boolean is58mm, int hight)
     * Bitmap  icon     жєђе›ѕ
     * String txt       и¦ЃиЅ¬жЌўзљ„е­—з¬¦дёІ
     * boolean is58mm   ж‰“еЌ°е®Ѕеє¦(58е’Њ80)
     * int hight        иЅ¬жЌўеђЋзљ„е›ѕз‰‡й«еє¦
     */
    private void GraphicalPrint(){

        String txt_msg = editText.getText().toString();
        if(txt_msg.length() == 0){
            Toast.makeText(PrintActivityNew.this, getText(R.string.empty1), Toast.LENGTH_SHORT).show();
            return;
        }else{
            Bitmap bm1 = getImageFromAssetsFile("demo.jpg");
            if(width_58mm.isChecked()){

                Bitmap bmp = Other.createAppIconText(bm1,txt_msg,25,is58mm,200);
                int nMode = 0;
                int nPaperWidth = 384;

                if(bmp != null)
                {
                    byte[] data = PrintPicture.POS_PrintBMP(bmp, nPaperWidth, nMode);
                    SendDataByte(Command.ESC_Init);
                    SendDataByte(Command.LF);
                    SendDataByte(data);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
                    SendDataByte(PrinterCommand.POS_Set_Cut(1));
                    SendDataByte(PrinterCommand.POS_Set_PrtInit());
                }
            }
            else if (width_80.isChecked()){
                Bitmap bmp = Other.createAppIconText(bm1,txt_msg,25,false,200);
                int nMode = 0;

                int nPaperWidth = 576;
                if(bmp != null)
                {
                    byte[] data = PrintPicture.POS_PrintBMP(bmp, nPaperWidth, nMode);
                    SendDataByte(Command.ESC_Init);
                    SendDataByte(Command.LF);
                    SendDataByte(data);
                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
                    SendDataByte(PrinterCommand.POS_Set_Cut(1));
                    SendDataByte(PrinterCommand.POS_Set_PrtInit());
                }
            }
        }
    }

    /**
     * ж‰“еЌ°жЊ‡д»¤жµ‹иЇ•
     */
    private void CommandTest(){

        String lang = getString(R.string.strLang);
        if((lang.compareTo("cn")) == 0){
            new AlertDialog.Builder(PrintActivityNew.this).setTitle(getText(R.string.chosecommand))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SendDataByte(byteCommands[which]);
                            try {
                                if(which == 16 || which == 17 || which == 18 || which == 19 || which == 22
                                        || which == 23 || which == 24|| which == 0 || which == 1 || which == 27){
                                    return ;
                                }else {
                                    SendDataByte("зѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\nзѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\nзѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\nзѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\nзѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\nзѓ­ж•ЏзҐЁжЌ®ж‰“еЌ°жњєABCDEFGabcdefg123456,.;'/[{}]!\n".getBytes("GBK"));
                                }

                            } catch (UnsupportedEncodingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }).create().show();
        }else if((lang.compareTo("en")) == 0){
            new AlertDialog.Builder(PrintActivityNew.this).setTitle(getText(R.string.chosecommand))
                    .setItems(itemsen, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SendDataByte(byteCommands[which]);
                            try {
                                if(which == 16 || which == 17 || which == 18 || which == 19 || which == 22
                                        || which == 23 || which == 24|| which == 0 || which == 1 || which == 27){
                                    return ;
                                }else {
                                    SendDataByte("Thermal Receipt Printer ABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\nThermal Receipt PrinterABCDEFGabcdefg123456,.;'/[{}]!\n".getBytes("GBK"));
                                }

                            } catch (UnsupportedEncodingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }).create().show();
        }
    }
    /************************************************************************************************/
	/*
	 * з”џж€ђQRе›ѕ
	 */
    private void createImage() {
        try {
            // йњЂи¦Ѓеј•е…ҐzxingеЊ…
            QRCodeWriter writer = new QRCodeWriter();

            String text = editText.getText().toString();

            Log.i(TAG, "з”џж€ђзљ„ж–‡жњ¬пјљ" + text);
            if (text == null || "".equals(text) || text.length() < 1) {
                Toast.makeText(this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                return;
            }

            // жЉЉиѕ“е…Ґзљ„ж–‡жњ¬иЅ¬дёєдєЊз»ґз Ѓ
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE,
                    QR_WIDTH, QR_HEIGHT);

            System.out.println("w:" + martix.getWidth() + "h:"
                    + martix.getHeight());

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }

                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.ARGB_8888);

            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);

            byte[] data = PrintPicture.POS_PrintBMP(bitmap, 384, 0);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
    //************************************************************************************************//
  	/*
  	 * и°ѓз”Ёзі»з»џз›ёжњє
  	 */
    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, actionCode);
    }

    private void handleSmallCameraPhoto(Intent intent) {
        Bundle extras = intent.getExtras();
        Bitmap mImageBitmap = (Bitmap) extras.get("data");
        imageViewPicture.setImageBitmap(mImageBitmap);
    }
/****************************************************************************************************/
    /**
     * еЉ иЅЅassetsж–‡д»¶иµ„жєђ
     */
    private Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;

    }

}