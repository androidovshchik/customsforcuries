package ru.newpartner.forcuriers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import ru.newpartner.forcuriers.R;
import zj.com.command.sdk.PrintPicture;
import zj.com.command.sdk.PrinterCommand;

//для печати
//конец для печати

public class PrintCheckActivity extends Activity{

    // Debugging
    private static final String TAG = "PrintCheckActivity";
    private static final boolean DEBUG = true;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    /*******************************************************************************************************/
    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CHOSE_BMP = 3;
    private static final int REQUEST_CAMER = 4;

    //QRcode
    private static final int QR_WIDTH = 350;
    private static final int QR_HEIGHT = 350;

    //длна строки в символах
    private static final int STR_WIDTH = 30;

    /*******************************************************************************************************/
    private static final String RUS = "CP1251";

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    private BluetoothService mService = null;

    // will show the statuses like bluetooth open, close or data sent
    TextView myLabel;

    // will enable user to enter any text to be printed
    TextView myTextbox;

    // android built in classes for bluetooth operations
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    //для получения данных из базы данных
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    Cursor userCursorGoods;
    Cursor userCursorNDS;

    //для сохранения состояния был ли уже распечатан чек при открытии активити
    int toPrint;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_check);

        Intent intentDoc = getIntent();
        String tekCheckNumber = intentDoc.getStringExtra("FD");

        if (savedInstanceState != null && savedInstanceState.containsKey("toPrint"))
            toPrint = savedInstanceState.getInt("toPrint");
        else toPrint = 1;

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Блютуз адаптер не определен, печать на принтере не возможна!",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        try {

            // more codes will be here
            // we are going to have three buttons for specific functions
            Button openButton = (Button) findViewById(R.id.open);
            Button sendButton = (Button) findViewById(R.id.send);
            Button closeButton = (Button) findViewById(R.id.close);

            // text label and input box
            myLabel = (TextView) findViewById(R.id.label);
            myTextbox = (TextView) findViewById(R.id.entry);

            //заполнение формы реквизитами чека
            sqlHelper = new DatabaseHelper(getApplicationContext());
            // создаем базу данных
            sqlHelper.create_db();
            sqlHelper.open();
            userCursor = sqlHelper.database.rawQuery("select Checks.* from Checks" +
                    " where FD = ?", new String[]{tekCheckNumber});
            userCursorGoods = sqlHelper.database.rawQuery("select ChecksGoods.* from ChecksGoods" +
                    " where CheckFD = ?", new String[]{tekCheckNumber});
            userCursorNDS = sqlHelper.database.rawQuery("select ChecksGoods.GoodsNDS, sum(ChecksGoods.GoodsSumNDS) as GoodsSumNDS, sum(ChecksGoods.GoodsSum) as GoodsSum from ChecksGoods" +
                    " where CheckFD = ?"+" group by GoodsNDS order by GoodsNDS", new String[]{tekCheckNumber});
            if (userCursor.getCount() > 0)
            {
                //печатаем на принтере
                userCursor.moveToFirst();
                String StrForPrint = "";
                StrForPrint = StrForPrint+"  КАССОВЫЙ ЧЕК, ФД: "+tekCheckNumber;
                myTextbox.setText(StrForPrint);

            }

            // open bluetooth connection
            openButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                       // findBT();
                        openBT();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });

            // send data typed by the user to be printed
            sendButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        //sendData();
                        if (mService != null) {
                            if (mService.getState() != BluetoothService.STATE_CONNECTED) {
                                Toast.makeText(PrintCheckActivity.this, R.string.not_connected, Toast.LENGTH_SHORT)
                                        .show();
                                return;
                            }
                            sendCheckToPrint(userCursor, userCursorGoods, userCursorNDS);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });

            // close bluetooth connection
            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        closeBT();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });

        }catch(Exception e) {
            e.printStackTrace();
        }

        //попробуем сразу распечатать на принтер
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (mService == null)  //ОЛ заремила пока
                KeyListenerInit();//з›‘еђ¬  //ОЛ заремила пока
            //ищем сразу принтер для печати
            try {
                findBT();
                //если нашли принтер, то сразу на нем печатаем
                //если не нашли, то открываем список устройств для выбора
                if (mmDevice != null) {
                    mService.connect(mmDevice);
                    myLabel.setText("Установлена связь с принтером: " + mmDevice.getAddress().toString());
                    if (toPrint == 1)
                    {
                    try {
                        TimeUnit.SECONDS.sleep(5);  //без этой задержки не успевает видимо подключиться к устройству
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    sendCheckToPrint(userCursor, userCursorGoods, userCursorNDS);
                        toPrint = 0;
                    }
                } else {
                    openBT();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    // this will send text data to be printed by the bluetooth printer
    void sendCheckToPrint(Cursor TekuserCursor, Cursor TekuserCursorGoods, Cursor TekuserCursorNDS) throws IOException {
        // the text typed by the user
        //DecimalFormat myFormatter = new DecimalFormat("###.##");
       String StrFormat = "%.2f%n";
        String StrFormat0 = "%.0f";
       //char Pref = (char)8803;
        String Pref = "=";
        if (TekuserCursor.getCount() > 0)
        {
            TekuserCursor.moveToFirst();
            //String msg = myTextbox.getText().toString();
            SendDataByte(PrinterCommand.POS_Set_PrintWidth(255));
           // SendDataByte(PrinterCommand.POS_Set_LeftSP(100));
            SendDataByte(PrinterCommand.POS_Set_Relative(100));
            String msg = "КАССОВЫЙ ЧЕК\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Relative(0));
            if (TekuserCursor.getInt(TekuserCursor.getColumnIndex("CheckType"))==1)
            {
            msg = "ПРИХОД\n";}
            else
            {
                msg = "ВОЗВРАТ ПРИХОДА\n";}
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            //печать по товарам и услугам
            int GoodsCount = TekuserCursorGoods.getCount();
            if (GoodsCount > 0)
            {
                String msg1;
                String msg2;
                Double msg12;
                for (int i=0; i<GoodsCount; i++) {
                    TekuserCursorGoods.moveToPosition(i);
                    SendDataByte(PrinterCommand.POS_Set_PrintWidth(170));
                    msg =  TekuserCursorGoods.getString(TekuserCursorGoods.getColumnIndex("GoodsName"));
                    SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Set_PrintWidth(255));
                    msg1 =  TekuserCursorGoods.getString(TekuserCursorGoods.getColumnIndex("SectionNumber"));
                    msg12 =  TekuserCursorGoods.getDouble(TekuserCursorGoods.getColumnIndex("GoodsSum"));
                    double TekCount = TekuserCursorGoods.getDouble(TekuserCursorGoods.getColumnIndex("GoodsCount"));
                    double TekPrice = TekuserCursorGoods.getDouble(TekuserCursorGoods.getColumnIndex("GoodsPrice"));
                    String msg3 = "";
                    if (TekCount > 1)
                    {
                       if (TekCount%1 == 0)
                           msg3  = " "+String.format(StrFormat0,TekCount)+"х"+String.format(StrFormat,TekPrice)+"  ";
                       else msg3  = " "+String.valueOf(TekCount)+"х"+String.format(StrFormat,TekPrice)+"  ";
                        msg3 = msg3.replace(",",".");
                    }
                    msg2 = String.format(StrFormat,msg12);
                    msg2 = Pref+msg2.replace(",",".");
                    if (msg3.isEmpty())
                    {
                        msg = formatString(msg1, msg2, 1);
                        SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
                    }
                    else
                    {
                        msg = formatString(msg1, msg3, 1);
                        SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
                        msg = formatString("", msg2, 1);
                        SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
                    }
                }
            }
            SendDataByte(PrinterCommand.POS_Set_PrintWidth(255));
            String msg1 = "ИТОГ";
            Double SumN = userCursor.getDouble(userCursor.getColumnIndex("CheckSumN"));
            Double SumE = userCursor.getDouble(userCursor.getColumnIndex("CheckSumE"));
            Double TotalSum = SumN+SumE;
            String msg2 = String.format(StrFormat,TotalSum);
            msg2 = msg2.replace(",",".");
            msg2 = Pref+msg2;
            msg = formatString(msg1, msg2, 2);
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 1, 2, 0));
            msg = "ОПЛАТА\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            if (SumN > 0) {
                msg1 = "НАЛИЧНЫМИ";
                msg2 = String.format(StrFormat,SumN);
                msg2 = Pref+msg2.replace(",",".");
                msg = formatString(msg1, msg2, 1);
                SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            }
            if (SumE > 0) {
                msg1 = "ЭЛЕКТРОННЫМИ";
                msg2 = String.format(StrFormat,SumE);
                msg2 = Pref+msg2.replace(",",".");
                msg = formatString(msg1, msg2, 1);
                SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            }
            msg = "ВСЕГО ОПЛАЧЕНО\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            msg1 = "НАЛИЧНЫМИ";
            msg2 = String.format(StrFormat,SumN);
            msg2 = Pref+msg2.replace(",",".");
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            msg1 = "ЭЛЕКТРОННЫМИ";
            msg2 = String.format(StrFormat,SumE);
            msg2 =Pref+ msg2.replace(",",".");
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));

            //печать суммы НДС
            GoodsCount = TekuserCursorNDS.getCount();
            int msg1_NDS;
            Double msg12;
            if (GoodsCount > 0)
            {
                for (int i=0; i<GoodsCount; i++) {
                    TekuserCursorNDS.moveToPosition(i);
                    msg1_NDS =  TekuserCursorNDS.getInt(TekuserCursorNDS.getColumnIndex("GoodsNDS"));
                    double msg1_sum =  TekuserCursorNDS.getInt(TekuserCursorNDS.getColumnIndex("GoodsSum"));
                    msg12 =  TekuserCursorNDS.getDouble(TekuserCursorNDS.getColumnIndex("GoodsSumNDS"));
                    if (msg12 > 0) {
                        msg2 = String.format(StrFormat, msg12);
                        if (msg1_NDS == 10)  msg2 = String.format(StrFormat, msg1_sum/110*10);
                        else if (msg1_NDS == 18)  msg2 = String.format(StrFormat, msg1_sum/118*18);
                        msg2 = Pref + msg2.replace(",", ".");
                        msg = formatString("Сумма НДС " + Integer.toString(msg1_NDS) + "%", msg2, 1);
                        SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
                    }
                }
            }

            msg1 = "Сайт ФНС:";
            msg2 = "www.nalog.ru";
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "Пользователь: ";
            msg2 = userCursor.getString(userCursor.getColumnIndex("Organization"));
           // msg = formatString(msg1, msg2, 1);
            msg = msg1+msg2;
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "Адрес: ";
            msg2 = userCursor.getString(userCursor.getColumnIndex("CheckAddress"));
            //msg = formatString(msg1, msg2, 1);
            msg = msg1+msg2;
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "Кассир: ";
            msg2 = userCursor.getString(userCursor.getColumnIndex("Cashier"));
            //msg = formatString(msg1, msg2, 1);
            msg = msg1+msg2;
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "ИНН:";
            msg2 = userCursor.getString(userCursor.getColumnIndex("INN"));
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "ЗН ККТ:";
            msg2 = userCursor.getString(userCursor.getColumnIndex("KktNumber"));
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "Смена №:";
            msg2 = userCursor.getString(userCursor.getColumnIndex("SmenaNumber"));
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "Чек №:";
            msg2 = userCursor.getString(userCursor.getColumnIndex("CheckNumber"));
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n\n", RUS, 73, 0, 0, 0));

            //фискальные данные

            msg = userCursor.getString(userCursor.getColumnIndex("CheckTime"));
            msg = "  "+msg;
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msg1 = "  ИТОГ";
            msg2 = String.format(StrFormat,TotalSum);
            msg2 = msg2.replace(",",".");
            msg2 = Pref+msg2+" ";
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            msg1 = " РН ККТ";
            msg2 = userCursor.getString(userCursor.getColumnIndex("KktRN"));
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            String msgQR = "t="+userCursor.getString(userCursor.getColumnIndex("CheckTimeStr"));
            msgQR = msgQR+"&s="+TotalSum;
            msg1 = "  ФН №";
            msg2 = userCursor.getString(userCursor.getColumnIndex("FN"))+" ";
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msgQR = msgQR+"&fn="+msg2;
            msg1 = "  ФД №";
            msg2 = userCursor.getString(userCursor.getColumnIndex("FD"))+" ";
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msgQR = msgQR+"&i="+msg2;
            msg1 = "  ФПД №";
            msg2 = userCursor.getString(userCursor.getColumnIndex("FPD"))+" ";
            msg = formatString(msg1, msg2, 1);
            SendDataByte(PrinterCommand.POS_Print_Text(msg+"\n", RUS, 73, 0, 0, 0));
            msgQR = msgQR+"&fp="+msg2;
            msgQR = msgQR+"&n="+userCursor.getString(userCursor.getColumnIndex("CheckType"));
            createImage(msgQR);
           // msg = userCursor.getString(userCursor.getColumnIndex("Organization"))+"\n\n\n";
            // SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_LF());
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());

        }
    }

    private String formatString(String TekStr1,String TekStr2,int koef)
    {
        int Kol = TekStr1.length()+TekStr2.length();
        Kol = STR_WIDTH/koef-Kol;
        String Strtrim = "";
        if (Kol > 0)
        {
            int k;
            for (k = 1; k<= Kol; k++) {Strtrim = Strtrim + " "; }
        }

        return TekStr1+Strtrim+TekStr2;
    }

    /************************************************************************************************/
	/*
	 * з”џж€ђQRе›ѕ
	 */
    private void createImage(String Tekmsg) {
        try {
            // йњЂи¦Ѓеј•е…ҐzxingеЊ…
            QRCodeWriter writer = new QRCodeWriter();

            String text = Tekmsg;

            Log.i(TAG, "з”џж€ђзљ„ж–‡жњ¬пјљ" + text);
            if (text == null || "".equals(text) || text.length() < 1) {
                Toast.makeText(this, getText(R.string.empty), Toast.LENGTH_SHORT).show();
                return;
            }

            // жЉЉиѕ“е…Ґзљ„ж–‡жњ¬иЅ¬дёєдєЊз»ґз Ѓ
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE,
                    QR_WIDTH, QR_HEIGHT);

            System.out.println("w:" + martix.getWidth() + "h:"
                    + martix.getHeight());

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }

                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.ARGB_8888);

           // bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);

            //byte[] data = PrintPicture.POS_PrintBMP(bitmap, 384, 0);
            byte[] data = PrintPicture.POS_PrintBMP(bitmap, 384, 0);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    // this will find a bluetooth printer device
    void findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if(mBluetoothAdapter == null) {
                myLabel.setText("Блютуз адаптер не найден");
            }

            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            myLabel.setText("Ищем принтер");
            int CountD = 0;
            if(pairedDevices.size() > 0) {
                myLabel.setText("Цикл по принтерам");
                for (BluetoothDevice device : pairedDevices) {
                    // myLabel.setText("Цикл по именам принтераов");
                    myLabel.setText(device.getName());
                    CountD++;
                    mmDevice = device;
                }
                    myLabel.setText("Нашли!!!");
                if (CountD == 1) {
                    myLabel.setText("Найден принтер: " + mmDevice.getAddress().toString());
                }
                else mmDevice = null;  //не знаем, на какой принтер печатать, пусть сами выбирают
                }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // tries to open a connection to the bluetooth printer device
    void openBT() throws IOException {
        Intent serverIntent = new Intent(PrintCheckActivity.this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    // close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            myLabel.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        if (mService != null) {

            if (mService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService.start();
            }
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (DEBUG)
            Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if (DEBUG)
            Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (mService != null)
            mService.stop();
        if (DEBUG)
            Log.e(TAG, "--- ON DESTROY ---");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("toPrint", toPrint);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        toPrint = savedInstanceState.getInt("toPrint");
    }

    /*****************************************************************************************************/
    private void KeyListenerInit() {

        mService = new BluetoothService(this, mHandler);
    }

    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (DEBUG)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            Print_Test();
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            //  mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            //  mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                case MESSAGE_CONNECTION_LOST:    //и“ќз‰™е·Іж–­ејЂиїћжЋҐ
                    Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_UNABLE_CONNECT:     //ж— жі•иїћжЋҐи®ѕе¤‡
                    Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:{
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras().getString(
                            DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        BluetoothDevice device = mBluetoothAdapter
                                .getRemoteDevice(address);
                        // Attempt to connect to the device
                        mService.connect(device);
                        myLabel.setText("Установлена связь с принтером: "+address);
                    }
                }
                break;
            }
            case REQUEST_ENABLE_BT:{
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    //ОЛ заремила
                    KeyListenerInit();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            }
            case REQUEST_CHOSE_BMP:{
                if (resultCode == Activity.RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.MediaColumns.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, opts);
                    opts.inJustDecodeBounds = false;
                    if (opts.outWidth > 1200) {
                        opts.inSampleSize = opts.outWidth / 1200;
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, opts);
                    if (null != bitmap) {
                       // imageViewPicture.setImageBitmap(bitmap); //ОЛ заремила пока
                    }
                }else{
                    Toast.makeText(this, getString(R.string.msg_statev1), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case REQUEST_CAMER:{
                if (resultCode == Activity.RESULT_OK){
                    //handleSmallCameraPhoto(data); //ОЛ заремила пока
                }else{
                    Toast.makeText(this, getText(R.string.camer), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    /****************************************************************************************************/
    /**
     * иїћжЋҐж€ђеЉџеђЋж‰“еЌ°жµ‹иЇ•йЎµ
     */
    private void Print_Test(){
            String msg = "Связь установлена!\n";
            String data = "----\n";
            SendDataByte(PrinterCommand.POS_Print_Text(msg, RUS, 73, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(data, RUS, 73, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
    }
    private void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        mService.write(data);
    }
}
