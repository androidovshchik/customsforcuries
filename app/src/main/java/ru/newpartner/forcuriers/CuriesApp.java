package ru.newpartner.forcuriers;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(mailTo = "@",
	mode = ReportingInteractionMode.DIALOG,
	resDialogText = R.string.crash_dialog_text,
	resDialogTheme = R.style.AppTheme_Dialog)
public class CuriesApp extends Application {

	@Override
	public final void onCreate() {
		super.onCreate();
		ACRA.init(this);
	}
}
