package ru.newpartner.forcuriers;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import ru.newpartner.forcuriers.R;

public class Goods_ListMyAdapter extends SimpleCursorAdapter {

    private int layout;

    public Goods_ListMyAdapter(Context _context, int _layout, Cursor _cursor, String[] _from, int[] _to) {
        super(_context, _layout, _cursor, _from, _to);
        layout = _layout;
      }

    //связывает данные с view на которые указывает курсор
    @Override
    public void bindView(View view, Context _context, Cursor _cursor) {
        String GoodsName = _cursor.getString(_cursor.getColumnIndex("GoodsName"));
        String GoodsCount = _cursor.getString(_cursor.getColumnIndex("GoodsCount"));
        String GoodsPrice = _cursor.getString(_cursor.getColumnIndex("GoodsPrice"));
        String GoodsSum = _cursor.getString(_cursor.getColumnIndex("GoodsSum"));
        TextView nameTV = (TextView) view.findViewById(R.id.TVname);
        TextView sumTV = (TextView) view.findViewById(R.id.TVSum);
        TextView countTV = (TextView) view.findViewById(R.id.TVcount);
        TextView priceTV = (TextView) view.findViewById(R.id.TVprice);

        nameTV.setText(GoodsName);
        sumTV.setText(GoodsSum);
        countTV.setText(GoodsCount);
        priceTV.setText(GoodsPrice);
    }

    //сoздаёт новую view для хранения данных на которую указывает курсор
    @Override
    public View newView(Context _context, Cursor _cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout, parent, false);
        return view;
    }


}
