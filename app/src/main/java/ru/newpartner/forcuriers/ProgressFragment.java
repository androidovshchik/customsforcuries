package ru.newpartner.forcuriers;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.AsyncTask;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import ru.newpartner.forcuriers.R;

public class ProgressFragment extends Fragment {
    TextView contentView;
    String contentText = null;
    WebView webView;
    String qq;

    private static final String NAMESPACE = "http://delivery-russia.ru";
      private static final String URL = "http://91.196.34.30/sd_ysd/ws/MobileCouriersExchange.1cws";
    private static final String SOAP_ACTION = "http://delivery-russia.ru#MobileCouriersExchange:GetCouriersList";
     private static final String METHOD_NAME = "GetCouriersList";
    private static final String user = "MobiUser";
    private static final String pass = "1597534682";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_progress, container, false);
        contentView = (TextView) view.findViewById(R.id.content);
        webView = (WebView) view.findViewById(R.id.webView);

        // если данные ранее были загружены
        if (contentText != null) {
            contentView.setText(contentText);
            webView.loadData(contentText, "text/html; charset=utf-8", "utf-8");
        }

        Button btnFetch = (Button) view.findViewById(R.id.loginBtn);
        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(contentText==null){
                    contentView.setText("Загрузка...");
//                   // new ProgressTask().execute("https://developer.android.com/index.html");
//                   // new ProgressTask().execute("https://prof.veeroute.com/rest/2/authentication/createSession?accountID=newpartner&user=newpartner.admin&password=admin731");
                    Log.i("","Я ЗДЕСЬ  ПРИВЕТ!!!");
                    new ProgressTask().execute("https://delivery-russia.ru/");
                    Log.i("","Я ЗДЕСЬ  ПОКА!!!");
                }
               // qq = getContent();
            }
        });
        return view;
    }



    private class ProgressTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content;
            content = "ПУСТО !";
            Log.e("","Я ЗДЕСЬ  30000!!!");
           // try {
                Log.i("", "Я ЗДЕСЬ 1 !!!");
                content = getContent("22222");
//            }
//            catch (IOException ex){
//                Log.i("","Я НЕ ЗДЕСЬ 1000 !!!");
//               // content = ex.getMessage();
//                Log.i("","Я ЗДЕСЬ 1000 !!!");
//            }
//            catch (XmlPullParserException e) {
//                e.printStackTrace();
//                content = e.getMessage();
//            }
            //content = "НЕ ПУСТО !";
            return content;
        }
        @Override
        protected void onPostExecute(String content) {

            contentText=content;
            contentView.setText(content);
            webView.loadData(content, "text/html; charset=utf-8", "utf-8");
            Toast.makeText(getActivity(), "Данные загружены", Toast.LENGTH_SHORT)
                    .show();
        }

        private String getContent(String path)  {

            String result = "";
            Log.i("","Я ЗДЕСЬ 2 !!!");
            do {
                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                String method = "GetCouriersList";
                SoapObject request1 = new SoapObject(NAMESPACE, method);

                request1.addProperty("Kod", "000000000");
                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request1);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
               // envelope.env = "http://www.w3.org/2001/XMLSchema";
                HttpTransportSE androidHttpTransport = new AuthTransportSE(URL, user, pass);
               // HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject result1 = (SoapObject) envelope.getResponse();
                    result = result1.toString();
                    Log.i("","Я ЗДЕСЬ УРА  !!!");
                } catch (Exception e) {
                    e.printStackTrace();
                    result = e.toString();
                    Log.i("","Я ЗДЕСЬ  НЕ  УРА  !!!");
                }
            }
            while (false);
            return result;
        }
    }

}




