package ru.newpartner.forcuriers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

	public static final String EXTRA_BAR_CODE = "barcode";

	private ZXingScannerView scannerView;

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.activity_scanner);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar supportActionBar = getSupportActionBar();
		if (supportActionBar != null) {
			supportActionBar.setDisplayHomeAsUpEnabled(true);
		}
		ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
		scannerView = new ZXingScannerView(getApplicationContext());
		scannerView.setResultHandler(this);
		scannerView.setAutoFocus(true);
		contentFrame.addView(scannerView);
	}

	@Override
	public void onResume() {
		super.onResume();
		scannerView.startCamera();
	}

	@Override
	public void onPause() {
		super.onPause();
		scannerView.stopCamera();
	}

	@Override
	public void handleResult(Result rawResult) {
		String barCode = rawResult.getText();
		if (barCode == null || barCode.trim().isEmpty()) {
			return;
		}
		scannerView.resumeCameraPreview(this);
		Intent intent = new Intent();
		intent.putExtra(EXTRA_BAR_CODE, rawResult.getText());
		setResult(RESULT_OK, intent);
		finish();
	}
}