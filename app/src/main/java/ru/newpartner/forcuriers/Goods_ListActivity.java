package ru.newpartner.forcuriers;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import ru.newpartner.forcuriers.R;

import static ru.newpartner.forcuriers.MainActivity.TekCurier_FIO;
import static ru.newpartner.forcuriers.LoginFragment.tekLogin;
import static ru.newpartner.forcuriers.MainActivity.NAMESPACE;
import static ru.newpartner.forcuriers.MainActivity.URL;
import static ru.newpartner.forcuriers.MainActivity.user;
import static ru.newpartner.forcuriers.MainActivity.pass;

import static ru.newpartner.forcuriers.MainActivity.loadCheck;

public class Goods_ListActivity extends ActionBarActivity {

    ListView mList;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    SimpleCursorAdapter userAdapter;
    String tekDoc_Number;
    int tekDoc_isGoods;
    TextView TxtSum;
    public Cursor tekGoodStr;
    Button btnNewCheck;

    public static final String SOAP_ACTION = "http://delivery-russia.ru#MobileCouriersExchange:SetNewCheck";
    public static final String METHOD_NAME_LOAD = "SetNewCheck";
    public static final String METHOD_NAME_GETCHECK = "GetCheck";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_list);

        Intent intentShop = getIntent();
        tekDoc_Number = intentShop.getStringExtra("DocNumber");
        tekDoc_isGoods = intentShop.getIntExtra("isGoods",0);

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        mTxtFIO.setText(TekCurier_FIO.toString());

        TextView mTxtDocNumber = (TextView) findViewById(R.id.textViewDocNumber);
        if (tekDoc_isGoods ==0)
            mTxtDocNumber.setText("Товары по накладной № "+tekDoc_Number);
        else
            mTxtDocNumber.setText("Услуги по накладной № "+tekDoc_Number);

        TxtSum = (TextView) findViewById(R.id.textViewDocSum);

        mList = (ListView) findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                tekGoodStr = (Cursor) mList.getItemAtPosition(position);
                //выделение строки цветом
                for(int a = 0; a < parent.getChildCount(); a++)
                {
                        LinearLayout TekViewStr = (LinearLayout) parent.getChildAt(a);
                        for (int a1 = 0; a1 < TekViewStr.getChildCount(); a1++) {
                            LinearLayout TekViewStr1 = (LinearLayout) TekViewStr.getChildAt(a1);
                            for (int a2 = 0; a2 < TekViewStr1.getChildCount(); a2++) {
                                if (a == position)
                                    TekViewStr1.getChildAt(a2).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_list_edit_style));
                                else
                                    TekViewStr1.getChildAt(a2).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_list_style));
                            }
                        }
                }
               //конец выделение строки цветом
                if (tekDoc_isGoods == 1)  showPopupMenu(view);  //можно менять кол-во только для товаров
            }
        });

        btnNewCheck = (Button) findViewById(R.id.NewCheckBtn);
        btnNewCheck.setOnClickListener((new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //отсылаем запрос к веб-сервису
                        //чтобы там пробился чек
                        String TekText = btnNewCheck.getText().toString();
                        if (TekText.contains("распечатать на принтере"))
                        { //чек уже создан в 1с и пробит там, печатаем на термопринтере
                            String tekNumber = btnNewCheck.getHint().toString();
                            Intent intent = new Intent(getApplicationContext(), PrintCheckActivity.class);
                            intent.putExtra("FD", tekNumber);
                            startActivity(intent);
                        }
                        else {
                            btnNewCheck.setText("Выполняется запрос на пробитие чека...");
                            btnNewCheck.setEnabled(false);
                            SendCheck SendTask = new SendCheck();
                            SendTask.execute();
                        }
                    }
                })

        );

        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

    }

    private void showPopupMenu(View v) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.menu_edit_count);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Toast.makeText(PopupMenuDemoActivity.this,
                        // item.toString(), Toast.LENGTH_LONG).show();
                        // return true;
                        switch (item.getItemId()) {

                            case R.id.MenuMinus:
                                EditCount(-1);
                                Toast.makeText(getApplicationContext(),
                                        "Вы выбрали Уменьшение товара",
                                        Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.MenuDelete:
                                EditCount(0);
                                Toast.makeText(getApplicationContext(),
                                        "Вы выбрали Удаление товара",
                                        Toast.LENGTH_SHORT).show();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
        popupMenu.show();
    }

    private void EditCount(int TekAction) {

        double tekCount = tekGoodStr.getDouble(tekGoodStr.getColumnIndex("GoodsCount"));
        long TekId =  tekGoodStr.getLong(tekGoodStr.getColumnIndex("_id"));
        Cursor userCursorCheck = sqlHelper.database.rawQuery("select CurrentCheck.* from CurrentCheck"
                +" where _id = "+TekId, null);
        int count = userCursorCheck.getCount();
        if (count > 0) {
            SQLiteDatabase dbCheck = sqlHelper.getWritableDatabase();
            if (TekAction==-1 & tekCount>1) {
                //заменяем запись в таблице на с меньшим кол-вом
                for (int i = 0; i < count; i++) {
                    userCursorCheck.moveToPosition(i);
                    ContentValues cv = new ContentValues();
                    cv.put("_id", TekId);
                    cv.put("DocNumber", userCursorCheck.getString(userCursorCheck.getColumnIndex("DocNumber")));
                    cv.put("GoodsName", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsName")));
                    Double NewCount = tekCount-1;
                    Double NewSum = userCursorCheck.getDouble(userCursorCheck.getColumnIndex("GoodsPrice"))*NewCount;
                    Double NewSumNDS =  userCursorCheck.getDouble(userCursorCheck.getColumnIndex("GoodsSumNDS"))/tekCount*NewCount;
                    cv.put("GoodsCount", NewCount);
                    cv.put("GoodsPrice", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsPrice")));
                    cv.put("GoodsSum", NewSum);
                    cv.put("GoodsNDS", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsNDS")));
                    cv.put("GoodsSumNDS", NewSumNDS);
                    cv.put("StrNumber", userCursorCheck.getString(userCursorCheck.getColumnIndex("StrNumber")));
                    dbCheck.update("CurrentCheck", cv, "_id = " + TekId, null);
                }
            }
            else
            {
               //удаляем запись из таблицы
                dbCheck.delete("CurrentCheck", "_id="+TekId, null);
            }
            dbCheck.close();
            UpdateList();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        UpdateList();
    }

    public void UpdateList() {

        try {

            sqlHelper.open();

            userCursor = sqlHelper.database.rawQuery("select CurrentCheck.* from CurrentCheck"
                    +" where DocNumber = ?"+" order by StrNumber", new String[]{tekDoc_Number});

            String[] from = {"GoodsName", "GoodsCount", "GoodsPrice", "GoodsSum"};
            int[] to = {R.id.TVname, R.id.TVcount, R.id.TVprice, R.id.TVSum};
            userAdapter = new Goods_ListMyAdapter(this, R.layout.layout_goods_list, userCursor, from, to);
            mList.setAdapter(userAdapter);

            //расчет суммы товаров
            Cursor userCursorItog = sqlHelper.database.rawQuery("select sum(CurrentCheck.GoodsSum) as GoodsSum from CurrentCheck"
                    +" where DocNumber = ?"+" order by StrNumber", new String[]{tekDoc_Number});
            if (userCursorItog.getCount()==0)
            {
                TxtSum.setText("Товаров на сумму: 0 руб.");
                btnNewCheck.setEnabled(false);
            }
            else {
                userCursorItog.moveToFirst();
                TxtSum.setText("Товаров на сумму: "+String.valueOf(userCursorItog.getDouble(userCursorItog.getColumnIndex("GoodsSum")))+" руб.");
                if (userCursorItog.getDouble(userCursorItog.getColumnIndex("GoodsSum")) > 0)
                    btnNewCheck.setEnabled(true);
                else
                    btnNewCheck.setEnabled(false);
            }
        }
        catch (SQLException ex) {
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    //объявдение класса для отсылки запроса к веб-сервису
    private class SendCheck extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content;
            if (isOnline() == true) {
                //запрос к веб-сервису для проверки логина и пароля
                content = SendCheckToServer();
                return content;
            } else {
                content = "Ошибка: Нет доступа к интернету, чек не может быть пробит!";
                return content;
            }
        }

        @Override
        protected void onPostExecute(String content) {

            Toast.makeText(getApplicationContext(), content, Toast.LENGTH_LONG)
                    .show();
            if (content.contains("Загружен пробитый чек"))
            {
                String TekFD = content.replace("Загружен пробитый чек","");
                TekFD = TekFD.replace(" ","");
                btnNewCheck.setText("Чек пробит в 1С и загружен, ожидайте, идет установка связи с принтером...");
                btnNewCheck.setHint(TekFD);
                btnNewCheck.setEnabled(true);
                //сразу отправляем чек на печать
                Intent intent = new Intent(getApplicationContext(), PrintCheckActivity.class);
                intent.putExtra("FD", TekFD);
                startActivity(intent);
            }
            else if (content.contains("Создан новый чек ККМ"))
            {
                btnNewCheck.setText("Чек создан в 1С, ожидание пробития в 1С...");
                btnNewCheck.setEnabled(false);
                btnNewCheck.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_checkwaiting_style));
                //запускаем сервис для опроса web-сервера
                //Intent IntetnServise = new Intent(Goods_ListActivity.this, ServiceQueryToServer.class);
                //IntetnServise.putExtra("DocNumber", tekDoc_Number);
                //startService(IntetnServise);
                //запускаем в другом потоке опрос сервера пока не будет получен пробитый чек
                GetCheck GetTask = new GetCheck();
                GetTask.execute();


            }
            else
            {
                btnNewCheck.setText("Неудача! Пробить чек в 1С");
                btnNewCheck.setEnabled(true);
            }

        }

        private String SendCheckToServer()  {

            String result = "";
            String answer_res = "-2"; //статус ответа -2 - ничего не получилось, FD - вернули AL пробитого чека, -1 - создан в 1с чек, ожидающий пробития
            do {
                Cursor userCursorSend = sqlHelper.database.rawQuery("select CurrentCheck.* from CurrentCheck"
                        +" where DocNumber = ?"+" order by StrNumber", new String[]{tekDoc_Number});
                int Count = userCursorSend.getCount();
                SoapObjectCustom request = new SoapObjectCustom(NAMESPACE, METHOD_NAME_LOAD);
                String StrReques = "";
                JSONObject Goods = new JSONObject();
                for (int i = 0; i < Count; i++) {
                    userCursorSend.moveToPosition(i);
                    JSONObject TekGood = new JSONObject();
                    try {
                        TekGood.put("GoodsName", userCursorSend.getString(userCursorSend.getColumnIndex("GoodsName")));
                        TekGood.put("GoodsCount", userCursorSend.getDouble(userCursorSend.getColumnIndex("GoodsCount")));
                        TekGood.put("GoodsPrice", userCursorSend.getDouble(userCursorSend.getColumnIndex("GoodsPrice")));
                        TekGood.put("GoodsSum", userCursorSend.getDouble(userCursorSend.getColumnIndex("GoodsSum")));
                        TekGood.put("GoodsNDS", userCursorSend.getDouble(userCursorSend.getColumnIndex("GoodsNDS")));
                        TekGood.put("GoodsSumNDS", userCursorSend.getDouble(userCursorSend.getColumnIndex("GoodsSumNDS")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        Goods.put("Good_"+Integer.toString(i),TekGood);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                JSONObject Docs = new JSONObject();
                try {
                    Docs.put("DocNumber",tekDoc_Number);
                    Docs.put("isGoods",tekDoc_isGoods);
                    Docs.put("Goods",Goods);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                StrReques = Docs.toString();
                request.addProperty("Kod",tekLogin);
                request.addProperty("ListOfDocs",StrReques);

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(URL, user, pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultCheck = (SoapObject) envelope.getResponse();
                    result = resultCheck.toString();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultCheck.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultCheck.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName"))
                        {
                            result = resultCheck.getProperty(i).toString();
                            if (result.contains("Создан новый чек ККМ")) {answer_res="-1";}
                            return result;
                        }
                    }
                    answer_res = loadCheck(resultCheck, tekDoc_Number, getApplicationContext());
                    if (!answer_res.equals("-2"))
                    {result = "Загружен пробитый чек "+answer_res;}
                    return result;

                } catch (Exception e) {
                    e.printStackTrace();
                    result = e.toString();
                }
            }
            while (false);
            return result;
        }

    }

    //объявление класса для получения чека из 1С
    //объявдение класса для отсылки запроса к веб-сервису
    private class GetCheck extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... path) {

            String content = "";
            int i = 1;
            while (i<6) {
                i++;
                if (isOnline() == true) {
                    //запрос к веб-сервису для проверки логина и пароля
                    content = GetCheckFromServer(tekDoc_Number);
                    if (content.contains("Загружен пробитый чек"))
                    {
                        i = 6;
                        return content;
                    }
                } else {
                }
                try {
                    TimeUnit.SECONDS.sleep(15);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return content;
        }

        @Override
        protected void onPostExecute(String content) {

            if (content.contains("Загружен пробитый чек")) {
                //запускаем печать чека
                String TekFD = content.replace("Загружен пробитый чек", "");
                TekFD = TekFD.replace(" ", "");
                btnNewCheck.setEnabled(true);
                btnNewCheck.setText("Чек загружен. Установка связи с принтером...");
                btnNewCheck.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_check_style));
                Intent intent = new Intent(getApplicationContext(), PrintCheckActivity.class);
                intent.putExtra("FD", TekFD);
                startActivity(intent);
            }
            else
            {
                //после нескольких попыток запросов к серверу не получили ответ
                btnNewCheck.setText("Не удалось пробить чек в 1С или сервер 1с не доступен. Повторите попытку позже.");
                btnNewCheck.setEnabled(true);            }

        }

        private String GetCheckFromServer(String DocNumber) {

            String result = "";
            String answer_res = "-2"; //статус ответа -2 - ничего не получилось, FD - вернули ФД пробитого чека, -1 - создан в 1с чек, ожидающий пробития

            do {
                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_GETCHECK);
                request.addProperty("DocNumber", DocNumber);

                MarshalBase64 mbase = new MarshalBase64();
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                envelope.implicitTypes = true;
                HttpTransportSE androidHttpTransport = new AuthTransportSE(URL, user, pass);
                androidHttpTransport.debug = true;

                try {
                    mbase.register(envelope);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultCheck = (SoapObject) envelope.getResponse();
                    result = resultCheck.toString();
                    PropertyInfo pi = new PropertyInfo();
                    int count = resultCheck.getPropertyCount();
                    for (int i = 0; i < count; i++) {
                        resultCheck.getPropertyInfo(i, pi);
                        if (pi.getName().equals("ErrorName"))
                        {
                            result = resultCheck.getProperty(i).toString();
                            if (result.contains("Создан новый чек ККМ")) {answer_res="-1";}
                            return result;
                        }
                    }
                    answer_res = loadCheck(resultCheck, DocNumber, getApplicationContext());
                    if (!answer_res.equals("-2"))
                    {result = "Загружен пробитый чек "+answer_res;}
                    return result;

                } catch (Exception e) {
                    e.printStackTrace();
                    result = e.toString();
                }
            }
            while (false);
            return result;
        }

    }

        protected boolean isOnline() {
        String cs = this.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                this.getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    public static class SoapObjectCustom extends SoapObject {

        public SoapObjectCustom(String namespace, String name) {
            super(namespace, name);
        }

        @Override
        public SoapObject addProperty(String name, Object value) {
            PropertyInfo propertyInfo = new PropertyInfo();
            propertyInfo.name = name;
            propertyInfo.type = value == null ? PropertyInfo.OBJECT_CLASS
                    : value.getClass();
            propertyInfo.setValue(value);

            // Добавил эту строку
            propertyInfo.setNamespace(this.namespace);

            return addProperty(propertyInfo);
        }
    }

}
