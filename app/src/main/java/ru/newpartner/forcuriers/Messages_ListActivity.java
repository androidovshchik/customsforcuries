package ru.newpartner.forcuriers;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;

import ru.newpartner.forcuriers.R;

public class Messages_ListActivity extends ActionBarActivity {

    ListView mList;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    SimpleCursorAdapter userAdapter;
    int tekdate;
    String tekML_Number;
    Cursor userCursorCheck;
    TextView KolDocs;
    int tekIsNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_messages);

        Intent intentMessages = getIntent();
        tekIsNew = intentMessages.getIntExtra("IsNew", 0);

        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        if (MainActivity.TekCurier_FIO == null)
                mTxtFIO.setText("Необходимо войти в приложение, указав логин и пароль !");
            else
                mTxtFIO.setText(MainActivity.TekCurier_FIO.toString());

        KolDocs = (TextView) findViewById(R.id.textViewKolMess);

        mList = (ListView) findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //если это новое сообщение, то даем возможность установки признака "Прочитано"
                Cursor tekstr = (Cursor) mList.getItemAtPosition(position);
                int MessageIsNew = tekstr.getInt(tekstr.getColumnIndex("IsNew"));
                if (MessageIsNew == 1) showPopupMenu(view, position);

                }

//                int tekdate = Integer.valueOf(tekdatestr);
//
//                Intent intent = new Intent(getApplicationContext(), DayShopsActivity.class);
//                intent.putExtra("tekdate", tekdate);
//                startActivity(intent);

        });

    }

    private void showPopupMenu(View v, final int tek_position) {

        final int tek_pos = tek_position;
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.menu_accept_message);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Toast.makeText(PopupMenuDemoActivity.this,
                // item.toString(), Toast.LENGTH_LONG).show();
                // return true;
                switch (item.getItemId()) {
                    case R.id.menu_accept_one:
                        AcceptMessages(1, tek_pos);
                        Toast.makeText(getApplicationContext(),
                                "Сообщение прочитано",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private void AcceptMessages(int TekAction, int tek_position) {

            Cursor tekstr = (Cursor) mList.getItemAtPosition(tek_position);
            Long tek_id = tekstr.getLong(tekstr.getColumnIndex("_id"));

            Cursor userCursor1 = sqlHelper.database.rawQuery("select * from Messages where _id = " + tek_id, null);
            if (userCursor1.getCount() > 0) {
                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                String sql = "update Messages set isNew = 0 where _id =" + tek_id + ";";
                db.execSQL(sql);
                db.close();
            }
        updateMessagesList();
    }


        @Override
    public void onResume() {
        super.onResume();

            updateMessagesList();


    }

    public void updateMessagesList() {

        try {
            sqlHelper.open();
            if (tekIsNew == 1) {
                userCursor = sqlHelper.database.rawQuery("select * from Messages" +
                        " where isNew = 1 and Curier_id = "+ MainActivity.TekCurier_id  + " order by DateTime DESC", null);
            }
            else
            {
                userCursor = sqlHelper.database.rawQuery("select * from Messages" +
                        " where Curier_id = "+ MainActivity.TekCurier_id  +" order by DateTime DESC", null);
            }
            String[] from = {"DateTimeStr", "Sender","MessageText"};
            int[] to = {R.id.TVdate, R.id.TVsender, R.id.TVText};
            userAdapter = new Messages_ListMyAdapter(this, R.layout.layout_messages, userCursor, from, to);
            mList.setAdapter(userAdapter);

            //выводим кол-во сообщений
            if (tekIsNew == 1) {
                KolDocs.setText("Кол-во новых сообщений: " + String.valueOf(userCursor.getCount()));
            }
            else {
                KolDocs.setText("Кол-во сообщений: " + String.valueOf(userCursor.getCount()));
            }


        }
        catch (SQLException ex) {
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
