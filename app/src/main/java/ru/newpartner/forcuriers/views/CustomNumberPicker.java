package ru.newpartner.forcuriers.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.widget.NumberPicker;

import ru.newpartner.forcuriers.R;

public class CustomNumberPicker extends NumberPicker {

	public CustomNumberPicker(Context context) {
		this(context, null);
	}

	public CustomNumberPicker(Context context, AttributeSet attrs) {
		super(new ContextThemeWrapper(context, R.style.NumberPickerStyle), attrs);
	}
}