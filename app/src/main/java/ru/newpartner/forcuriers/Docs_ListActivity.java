package ru.newpartner.forcuriers;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mobeta.android.dslv.DragSortListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ru.newpartner.forcuriers.MainActivity.isSortDocs;

public class Docs_ListActivity extends ActionBarActivity {

    private static final int REQUEST_BAR_CODE = 1;

    DragSortListView mList;
    DatabaseHelper sqlHelper;
    Cursor userCursor;
    Docs_ListMyAdapter userAdapter;
    int tekdate;
    String tekML_Number;
    Cursor userCursorCheck;
    TextView KolDocs;
    boolean WithoutPOD;
    MenuItem searchMenuItem;
    String search = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs_list);

        sqlHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных
        sqlHelper.create_db();

        Intent intentShop = getIntent();
        tekdate = intentShop.getIntExtra("tekdate", 0);
        tekML_Number = intentShop.getStringExtra("MLNumber");

        TextView mTxtFIO = (TextView) findViewById(R.id.textViewFIO);
        if (MainActivity.TekCurier_FIO != null)
            mTxtFIO.setText(MainActivity.TekCurier_FIO.toString());
        else
            mTxtFIO.setText("Курьер не выбран");

        TextView mTxtML = (TextView) findViewById(R.id.textViewML);
        mTxtML.setText("Маршр.лист "+ (tekML_Number == null ? "" : tekML_Number.toString()) +
            " от "+ MainActivity.IntDateToString(tekdate, true));

        isSortDocs = true;
        KolDocs = (TextView) findViewById(R.id.textViewKolDocs);

        mList = (DragSortListView) findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                showPopupMenu(v, position);
            }
        });
        mList.setDragSortListener(new DragSortListView.DragSortListener() {

            @Override
            public void drag(int from, int to) {}

            @Override
            public void drop(int from, int to) {
                if (to != from) {
                    MoveDoc(from, to - from);
                }
                mList.requestFocusFromTouch();
                mList.setSelection(to);
                mList.requestFocus();
                userCursor.requery();
            }

            @Override
            public void remove(int which) {}
        });

        SharedPreferences preferences =
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loopViews((ViewGroup) findViewById(R.id.layout), preferences.getInt("fontSize",
            Math.round(getResources().getDimensionPixelSize(R.dimen.textSize) /
                getResources().getDisplayMetrics().density)));
    }

    private void showPopupMenu(View v, final int TekPosition) {
        final Cursor tekstr = (Cursor) mList.getItemAtPosition(TekPosition);
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.menu_actions_docs);
        final String tekPhone = tekstr.getString(tekstr.getColumnIndex("Phone"));
        popupMenu.getMenu().findItem(R.id.MenuCall).setEnabled(tekPhone != null &&
            !tekPhone.trim().isEmpty());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.MenuCheck:
                        String tekNumber = tekstr.getString(tekstr.getColumnIndex("DocNumber"));
                        int tekisGoods = tekstr.getInt(tekstr.getColumnIndex("isGoods"));
                        //проверка, есть ли уже пробитый чек по этой накладной
                        userCursorCheck = sqlHelper.database.rawQuery("select Checks._id as _id, Checks.DocNumber as DocNumber from Checks" +
                            " where DocNumber = ?"+" order by DocNumber", new String[]{tekNumber});
                        if (userCursorCheck.getCount() == 0) {
                            //копируем во временную таблицу, чтобы потом отредактиповать там кол-во, если нужно
                            SQLiteDatabase db = sqlHelper.getWritableDatabase();
                            db.delete("CurrentCheck", "", null);
                            userCursorCheck = sqlHelper.database.rawQuery("select Goods.* from Goods where Goods.DocNumber = ?"+" order by StrNumber", new String[]{tekNumber});
                            int count = userCursorCheck.getCount();
                            if (count > 0) {
                                for (int i=0; i<count; i++) {
                                    userCursorCheck.moveToPosition(i);
                                    ContentValues cv = new ContentValues();
                                    cv.put("DocNumber", userCursorCheck.getString(userCursorCheck.getColumnIndex("DocNumber")));
                                    cv.put("GoodsName", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsName")));
                                    cv.put("GoodsCount", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsCount")));
                                    cv.put("GoodsPrice", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsPrice")));
                                    cv.put("GoodsSum", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsSum")));
                                    cv.put("GoodsNDS", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsNDS")));
                                    cv.put("GoodsSumNDS", userCursorCheck.getString(userCursorCheck.getColumnIndex("GoodsSumNDS")));
                                    cv.put("StrNumber", userCursorCheck.getString(userCursorCheck.getColumnIndex("StrNumber")));
                                    db.insert("CurrentCheck", null, cv);
                                }
                            }
                            db.close();
                            //открываем список с товарами, чтобы указать, какие нужно включить в чек
                            Intent intent = new Intent(getApplicationContext(), Goods_ListActivity.class);
                            intent.putExtra("DocNumber", tekNumber);
                            intent.putExtra("isGoods", tekisGoods);
                            startActivity(intent);
                        }
                        else {
                            //открываем список пробитых чеков, чтобы потом их распечатать на термопринтере
                            Intent intent = new Intent(getApplicationContext(), Checks_ListActivity.class);
                            intent.putExtra("DocNumber", tekNumber);
                            startActivity(intent);
                        }
                        return true;
                    case R.id.MenuEvent:
                        int TekPOD = tekstr.getInt(tekstr.getColumnIndex("POD"));
                        int TekZabor = tekstr.getInt(tekstr.getColumnIndex("DocIsDelivery"));
                        if (TekPOD == 1) {
                            Toast.makeText(getApplicationContext(),
                                "По данной накладной ПОД уже введен",
                                Toast.LENGTH_SHORT).show();
                        }
                        else if (TekPOD == 2) {
                            Toast.makeText(getApplicationContext(),
                                "По данной заявке уже передано подтверждение о принятии",
                                Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Intent intent = new Intent(getApplicationContext(), Docs_Set_ActionActivity.class);
                            intent.putExtra("DocNumber", tekstr.getString(tekstr.getColumnIndex("DocNumber")));
                            intent.putExtra("DocZNumber", tekstr.getString(tekstr.getColumnIndex("DocZNumber")));
                            intent.putExtra("DocIsDelivery", TekZabor);
                            startActivity(intent);
                        }
                        return true;
                    case R.id.MenuGeo:
                        String tekaddress = tekstr.getString(tekstr.getColumnIndex("Address"));
                        Uri uri = Uri.parse("yandexnavi://map_search").buildUpon()
                            .appendQueryParameter("text", tekaddress).build();
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("ru.yandex.yandexnavi");
                        // Проверяет, установлено ли приложение.
                        PackageManager packageManager = getPackageManager();
                        List<ResolveInfo> activities =
                            packageManager.queryIntentActivities(intent, 0);
                        boolean isIntentSafe = activities.size() > 0;
                        if (isIntentSafe) {
                            //Запускает Яндекс.Навигатор.
                            startActivity(intent);
                        } else {
                            // Открывает страницу Яндекс.Навигатора в Google Play.
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=ru.yandex.yandexnavi"));
                            startActivity(intent);
                        }
                        return true;
                    case R.id.MenuCall:
                        try {
                            Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
                            phoneIntent.setData(Uri.parse("tel:" + tekPhone));
                            startActivity(phoneIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getApplicationContext(),
                                "Не найдено приложения для совершения звонка",
                                Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    public void updateDocsList() {
        boolean AllDocs = !WithoutPOD;
        try {
            sqlHelper.open();
            String StrQuery = "select docs._id as _id, docs.DocNumber as DocNumber, docs.DocZNumber as DocZNumber, docs.Address as Address, docs.TripNumber as TripNumber, " +
                "docs.DocIsDelivery as DocIsDelivery, docs.POD as POD, docs.isGoods as isGoods, docs.FIO as FIO, docs.Phone as Phone, docs.Company as Company, Goods1.GoodsSum as GoodsSum from Docs " +
                "left join (select DocNumber, sum(GoodsSum) as GoodsSum from Goods group by DocNumber) as Goods1 on docs.DocNumber = Goods1.DocNumber " +
                "where MLDate = "+tekdate+" and MLNumber = ?";
            if (!AllDocs) {
                StrQuery = StrQuery + " and POD = 0";
            }
            if (!search.trim().isEmpty()) {
                StrQuery = StrQuery + " and (LOWER(docs.DocNumber) LIKE LOWER('%" + search + "%') OR LOWER(docs.DocZNumber) LIKE LOWER('%" + search + "%'))";
            }
            StrQuery = StrQuery + " order by TripNumber";
            userCursor = sqlHelper.database.rawQuery(StrQuery, new String[]{tekML_Number});
            userAdapter = new Docs_ListMyAdapter(this, R.layout.layout_docs_list, userCursor,
                new String[]{}, new int[]{});
            mList.setAdapter(userAdapter);
            //кол-во заданий
            Cursor userCursorCount = sqlHelper.database.rawQuery("select docs._id from Docs "+
                    "where MLDate = " + tekdate + (!AllDocs ? " and POD = 0" : "") + " and MLNumber = ?",
                new String[]{tekML_Number});
            //кол-во доставленных накладных
            Cursor userCursorPOD = sqlHelper.database.rawQuery("select docs._id from Docs "+
                "where MLDate = " + tekdate + " and POD = 1 and MLNumber = ?", new String[]{tekML_Number});
            //выводим кол-во накладных
            KolDocs.setText("Заданий: "+String.valueOf(userCursorCount.getCount()) +
                ", выполнено: "+String.valueOf(userCursorPOD.getCount()));
        }
        catch (SQLException ex) {
        }
    }

    public void MoveDoc(final int TekPosition, int direction) {

        if (direction < 0 && TekPosition > 0 || direction > 0 && TekPosition < mList.getCount()-1) {
            try {
                sqlHelper.open();
                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                //передвижение позиций всех между первой и второй позицией
                long Tekid;
                Cursor tekstr;
                if (direction > 0) {
                    //передвижение строки вниз
                    Tekid = mList.getItemIdAtPosition(TekPosition);
                    tekstr = sqlHelper.database.rawQuery("select Docs.TripNumber, Docs.DocNumber, Docs.DocZNumber from Docs where _id = " + Tekid, null);
                    if (tekstr.getCount() > 0) {
                        tekstr.moveToFirst();
                        int TripNumber;
                        TripNumber = tekstr.getInt(tekstr.getColumnIndex("TripNumber"));
                        for (int i = 1; i <= direction; i++) {
                            long Tekid1 = mList.getItemIdAtPosition(TekPosition + i);
                            Cursor tekstr1 = sqlHelper.database.rawQuery("select Docs.TripNumber, Docs.DocNumber, Docs.DocZNumber from Docs where _id = " + Tekid1, null);
                            if (tekstr1.getCount() > 0) {
                                tekstr1.moveToFirst();
                                int TripNumber1 = tekstr1.getInt(tekstr.getColumnIndex("TripNumber"));
                                //заменяем в текущей строке номер поездки на предыдущий, а этот сохраняем
                                String sql1 = "update Docs set TripNumber = " + TripNumber + ", TripNumberNew = 1 where _id =" + Tekid1 + ";";
                                db.execSQL(sql1);
                                TripNumber = TripNumber1;
                            }
                        }
                        //пердвигаемой позиции устанавливаем номер поездки из последней позиции цикла
                        String sql1 = "update Docs set TripNumber = " + TripNumber + ", TripNumberNew = 1 where _id =" + Tekid + ";";
                        db.execSQL(sql1);
                        db.close();
                    }
                }
                else if (direction < 0) {
                    //передвижение строки вверх
                    Tekid = mList.getItemIdAtPosition(TekPosition);
                    tekstr = sqlHelper.database.rawQuery("select Docs.TripNumber, Docs.DocNumber, Docs.DocZNumber from Docs where _id = " + Tekid, null);
                    if (tekstr.getCount() > 0) {
                        tekstr.moveToFirst();
                        int TripNumber;
                        TripNumber = tekstr.getInt(tekstr.getColumnIndex("TripNumber"));
                        for (int i = -1; i >= direction; i--) {
                            long Tekid1 = mList.getItemIdAtPosition(TekPosition + i);
                            Cursor tekstr1 = sqlHelper.database.rawQuery("select Docs.TripNumber, Docs.DocNumber, Docs.DocZNumber from Docs where _id = " + Tekid1, null);
                            if (tekstr1.getCount() > 0) {
                                tekstr1.moveToFirst();
                                int TripNumber1 = tekstr1.getInt(tekstr.getColumnIndex("TripNumber"));
                                //заменяем в текущей строке номер поездки на предыдущий, а этот сохраняем
                                String sql1 = "update Docs set TripNumber = " + TripNumber + ", TripNumberNew = 1 where _id =" + Tekid1 + ";";
                                db.execSQL(sql1);
                                TripNumber = TripNumber1;
                            }
                        }
                        //пердвигаемой позиции устанавливаем номер поездки из последней позиции цикла
                        String sql1 = "update Docs set TripNumber = " + TripNumber + ", TripNumberNew = 1 where _id =" + Tekid + ";";
                        db.execSQL(sql1);
                        db.close();
                    }
                }

            } catch (SQLException ex){}
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        updateDocsList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Закрываем подключения
        sqlHelper.database.close();
        userCursor.close();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("search", search);
        outState.putBoolean("only", WithoutPOD);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        search = savedInstanceState.getString("search");
        WithoutPOD = savedInstanceState.getBoolean("only");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_docs, menu);
        menu.findItem(R.id.only).setChecked(WithoutPOD);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        if (!search.trim().isEmpty()) {
            searchMenuItem.expandActionView();
            searchView.setQuery(search, false);
            searchView.clearFocus();
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search = newText.trim();
                updateDocsList();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setChecked(!item.isChecked());
        switch (item.getItemId()) {
            case R.id.only:
                WithoutPOD = item.isChecked();
                updateDocsList();
                break;
            case R.id.scanner:
                TedPermission.with(getApplicationContext())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            startActivityForResult(new Intent(getApplicationContext(),
                                ScannerActivity.class), REQUEST_BAR_CODE);
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {}
                    })
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
                break;
            case R.id.font:
                final SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                View layout = View.inflate(getApplicationContext(), R.layout.dialog_font_size, null);
                final NumberPicker numberPicker = (NumberPicker) layout.findViewById(R.id.numberPicker);
                numberPicker.setMinValue(1);
                numberPicker.setMaxValue(24);
                numberPicker.setValue(preferences.getInt("fontSize", Math.round(getResources()
                    .getDimensionPixelSize(R.dimen.textSize) / getResources().getDisplayMetrics().density)));
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Размер шрифта")
                    .setView(layout)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNegativeButton(android.R.string.cancel, null);
                final AlertDialog dialog = builder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(final DialogInterface dialogInterface) {
                        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        positiveButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                int fontSize = numberPicker.getValue();
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt("fontSize", fontSize).apply();
                                loopViews((ViewGroup) findViewById(R.id.layout), fontSize);
                                userAdapter.notifyDataSetChanged();
                                dialog.cancel();
                            }
                        });
                    }
                });
                dialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loopViews(ViewGroup view, int fontSize) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTextSize(fontSize);
            } else if (v instanceof ViewGroup && !(v instanceof DragSortListView)) {
                this.loopViews((ViewGroup) v, fontSize);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_BAR_CODE && resultCode == RESULT_OK) {
            if (data.getExtras() == null) {
                return;
            }
            search = data.getExtras().getString(ScannerActivity.EXTRA_BAR_CODE);
            if (searchMenuItem == null) {
                return;
            }
            searchMenuItem.expandActionView();
            SearchView searchView = (SearchView) searchMenuItem.getActionView();
            searchView.setQuery(search, false);
            searchView.clearFocus();
        }
    }
}
